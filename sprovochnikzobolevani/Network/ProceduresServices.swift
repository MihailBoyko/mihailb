//
//  Api.swift
//  sprovochnikzobolevani
//
//  Created by Apple on 03.07.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import Alamofire
import Gloss
import SQLite
import Localize_Swift




struct ProcedureItem: JSONDecodable {
    let id: Int?
    let name: String?
    let category: Int?
    
    
    init?(json: JSON) {
        if let idString: String = "id" <~~ json {
            self.id = Int(idString)
        } else {
            self.id = nil
        }
        self.name = "name" <~~ json
        if let categoryString: String = "category" <~~ json {
            self.category = Int(categoryString)
        } else {
            self.category = nil
        }
    }
}


struct Procedures: JSONDecodable {
    
    let status: Int?
    let error: String?
    let procedures: [ProcedureItem]?
    let is_update: Bool?
    
    init?(json: JSON) {
        self.status = "status" <~~ json
        self.error = "error" <~~ json
        self.procedures = "procedures" <~~ json
        self.is_update = "is_update" <~~ json
    }
    
}



final class ProcedureServices {
    
    
    
    private init() {
    }
    
    static func categories(_ after: @escaping (ApiResult, [Category]) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            return
        }
        
        let lang = Localize.currentLanguage()
        let timestamp: Int = 0
//        if !isWritten {
//            timestamp = 0
//        }else{
//            switch(lang){
//            case "ru": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.categoriesProcRuTimestamp")
//            case "en": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.categoriesProcEnTimestamp")
//            default:   timestamp = UserDefaults.standard.integer(forKey: "ru.involta.categoriesProcEnTimestamp")
//            }
//        }

        Alamofire.request("http://directorydiseases.ru/api6/procedures/getCategories", method: .post, parameters: ["timestamp": "\(timestamp)", "localization": lang]).validate().responseJSON { response in
            var results = [Category]()
            switch response.result {
            case .success(let json):
                if let categories = Categories(json: json as! JSON) {
                    results = (categories.categories?.map { Category(id: Int64($0.id!+1), name: $0.name!.uppercased()) })!
                }
                DispatchQueue.main.async {
                    after(.done, results)
                }
                break
            case .failure(_):
                DispatchQueue.main.async {
                    after(.noServerConnection, results)
                }
                break
            }
        }
//        if isWritten {
//            timestamp = Int(Date().timeIntervalSince1970.rounded())
//            switch(lang){
//            case "ru":  UserDefaults.standard.set(timestamp, forKey: "ru.involta.categoriesProcRuTimestamp")
//            case "en":  UserDefaults.standard.set(timestamp, forKey: "ru.involta.categoriesProcRuTimestamp")
//            default:    UserDefaults.standard.set(timestamp, forKey: "ru.involta.categoriesProcRuTimestamp")
//            }
//        }
    }
    
    static func procedures(_ after: @escaping (ApiResult, [Item]) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            return
        }
        
        let lang = Localize.currentLanguage()
        var timestamp: Int
        if !isWritten {
            timestamp = 0
        }else{
            switch(lang){
            case "ru": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.proceduresRuProcTimestamp")
            case "en": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.proceduresEnProcTimestamp")
            case "es": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.proceduresEsProcTimestamp")
            default:   timestamp = UserDefaults.standard.integer(forKey: "ru.involta.proceduresEnProcTimestamp")
            }
        }
        
        Alamofire.request("http://directorydiseases.ru/api6/procedures/getNames", method: .post, parameters: ["timestamp": "\(timestamp)", "localization": Localize.currentLanguage()]).validate().responseJSON { response in
            var results = [Item]()
            switch response.result {
            case .success(let json):
                if let procedures = Procedures(json: json as! JSON) {
                    results = (procedures.procedures?.map { Item(id: Int64($0.id!), categoryId: Int64($0.category!+1), name: $0.name!, type: .procedure, lang: Localize.currentLanguage()) })!
                }
                DispatchQueue.main.async {
                    after(.done, results)
                }
                break
            case .failure(_):
                DispatchQueue.main.async {
                    after(.noServerConnection, results)
                }
                break
            }
        }
        if isWritten {
            timestamp = Int(Date().timeIntervalSince1970.rounded())
            switch(lang){
            case "ru":  UserDefaults.standard.set(timestamp, forKey: "ru.involta.proceduresRuProcTimestamp")
            case "en":  UserDefaults.standard.set(timestamp, forKey: "ru.involta.proceduresEnProcTimestamp")
            case "es":  UserDefaults.standard.set(timestamp, forKey: "ru.involta.proceduresEsProcTimestamp")
            default:    UserDefaults.standard.set(timestamp, forKey: "ru.involta.proceduresEnProcTimestamp")
            }
        }
    }
    
    static func content(ID: Int, lang: String, _ after: @escaping (ApiResult, [Content]) -> Void){
        
        if !Connectivity.isConnectedToInternet {
            return
        }
        
        Alamofire.request("http://directorydiseases.ru/api6/procedures/getProcedureById", method: .post, parameters: ["id": "\(ID)", "localization": lang]).validate().responseJSON { response in
            var results = [Content]()
            switch response.result {
            case .success(let json):
                if let d = ContentApi(json: json as! JSON) {
                    if (d.status! == 1 ) {
                        if((d.description?.count)! > 0){
                            results.append(Content( itemId: Int64(ID), header: "Description".localized(), data: d.description!, lang: lang))
                        }
                        for b in d.blocks! {
                            if((b.data?.count)! > 0){
                                results.append(Content( itemId: Int64(ID), header: b.header!, data: b.data!, lang: lang))
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    after(.done, results)
                }
                break;
            case .failure(_):
                DispatchQueue.main.async {
                    after(.noServerConnection, results)
                }
                break
            }
        }
        
    }
    
}
