//
//  Api.swift
//  sprovochnikzobolevani
//
//  Created by Apple on 03.07.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import Alamofire
import Gloss
import SQLite
import Localize_Swift

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

enum ApiResult {
    case noServerConnection
    case databaseError
    case done
}

struct CategoryApi: JSONDecodable {
    
    let id: Int?
    let name: String?
    
    init?(json: JSON) {
        
        if let idString: String = "id" <~~ json {
            self.id = Int(idString)
        } else {
            self.id = nil
        }
        self.name = "name" <~~ json
    }
    
}

struct Categories: JSONDecodable {
    
    let status: Int?
    let error: String?
    let categories: [CategoryApi]?
    let is_update: Bool?
    
    init?(json: JSON) {
        self.status = "status" <~~ json
        self.error = "error" <~~ json
        self.categories = "categories" <~~ json
        self.is_update = "is_update" <~~ json
    }
    
}

struct Disease: JSONDecodable {
    let id: Int?
    let name: String?
    let category: Int?
    
    init?(json: JSON) {
        if let idString: String = "id" <~~ json {
            self.id = Int(idString)
        } else {
            self.id = nil
        }
        self.name = "name" <~~ json
        if let categoryString: String = "category" <~~ json {
            self.category = Int(categoryString)
        } else {
            self.category = nil
        }
    }
}
struct Diseases: JSONDecodable {
    
    let status: Int?
    let error: String?
    let diseases: [Disease]?
    let is_update: Bool?
    
    init?(json: JSON) {
        self.status = "status" <~~ json
        self.error = "error" <~~ json
        self.diseases = "diseases" <~~ json
        self.is_update = "is_update" <~~ json
    }
    
}

struct Block : JSONDecodable {
    
    let header: String?
    let data: String?
    
    
    init?(json: JSON) {
        self.header = "header" <~~ json
        self.data = "data" <~~ json
    }
    
}

struct ContentApi {
    
    let status: Int?
    let id: Int?
    let name: String?
    let category: Int?
    let description: String?
    let images: [String]?
    let blocks: [Block]?
    let images_dir: String?
    let symptoms: String?
    
    init?(json: JSON) {
        self.status = "status" <~~ json
        if let idString: String = "id" <~~ json {
            self.id = Int(idString)
        } else {
            self.id = nil
        }
        self.name = "name" <~~ json
        if let categoryString: String = "category" <~~ json {
            self.category = Int(categoryString)
        } else {
            self.category = nil
        }
        self.description = "description" <~~ json
        self.images = "images" <~~ json
        self.blocks = "blocks" <~~ json
        self.images_dir = "images_dir" <~~ json
        self.symptoms = "symptoms" <~~ json
    }
    
}

struct Post : JSONDecodable {
    let postTitle: String?
    let preview: String?
    let permalink: String?
    
    init?(json: JSON) {
        self.postTitle = "post_title" <~~ json
        self.preview = "preview" <~~ json
        self.permalink = "permalink" <~~ json
    }
}

struct Posts : JSONDecodable {
    let status: Int?
    let diseases: [Disease]?
    let posts: [Post]?
    let searchType: Int?
    let yandex: Bool?
    
    init?(json: JSON) {
        self.status = "status" <~~ json
        self.diseases = "diseases" <~~ json
        self.posts = "posts" <~~ json
        self.searchType = "search_type" <~~ json
        self.yandex = "yandex" <~~ json
    }
}



final class DiseaseServices {
    
    
    
    private init() {
    }
    
    static func categories(_ after: @escaping (ApiResult, [Category]) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            return
        }
        let lang = Localize.currentLanguage()
        var timestamp: Int
        if !isWritten || lang == "en" || lang == "es" {
            timestamp = 0
        }else {
            switch(lang){
                case "ru": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.categoriesRuDiseaseTimestamp")
                case "en": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.categoriesEnDiseaseTimestamp")
                case "es": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.categoriesEsDiseaseTimestamp")
                default:   timestamp = UserDefaults.standard.integer(forKey: "ru.involta.categoriesEnDiseaseTimestamp")
            }
        }
        
        Alamofire.request("http://directorydiseases.ru/api6/disease/getCategories", method: .post, parameters: ["timestamp": "\(timestamp)", "localization": Localize.currentLanguage()]).validate().responseJSON { response in
            var results = [Category]()
            switch response.result {
            case .success(let json):
                if let categories = Categories(json: json as! JSON) {
                    results = (categories.categories?.map { Category(id: Int64($0.id!), name: Localize.currentLanguage() == "ru" ?  $0.name! : $0.name!.uppercased()) })!
                }
                DispatchQueue.main.async {
                    after(.done, results)
                }
                break
            case .failure(_):
                DispatchQueue.main.async {
                    after(.noServerConnection, results)
                }
                break
            }
        }
        if isWritten && lang == "ru" {
            timestamp = Int(Date().timeIntervalSince1970.rounded())
            switch(lang){
            case "ru": UserDefaults.standard.set(timestamp, forKey: "ru.involta.categoriesRuDiseaseTimestamp")
            case "en": UserDefaults.standard.set(timestamp, forKey: "ru.involta.categoriesEnDiseaseTimestamp")
            case "es": UserDefaults.standard.set(timestamp, forKey: "ru.involta.categoriesEsDiseaseTimestamp")
            default:   UserDefaults.standard.set(timestamp, forKey: "ru.involta.categoriesEnDiseaseTimestamp")
            }
        }
    }
    
    static func diseases(_ after: @escaping (ApiResult, [Item]) -> Void) {

        if !Connectivity.isConnectedToInternet {
            return
        }
        
        let lang = Localize.currentLanguage()
        var timestamp: Int
        if !isWritten {
            timestamp = 0
        }else{
            switch(lang){
            case "ru": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.diseasesRuTimestamp")
            case "en": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.diseasesEnTimestamp")
            case "es": timestamp = UserDefaults.standard.integer(forKey: "ru.involta.diseasesEsTimestamp")
            default:   timestamp = UserDefaults.standard.integer(forKey: "ru.involta.diseasesEnTimestamp")
            }
        }
        
        Alamofire.request("http://directorydiseases.ru/api6/disease/getNames", method: .post, parameters: ["timestamp": "\(timestamp)", "localization": lang]).validate().responseJSON { response in
            var results = [Item]()
            switch response.result {
                case .success(let json):
                    if let diseases = Diseases(json: json as! JSON) {
                        results = (diseases.diseases?.map { Item(id: Int64($0.id!), categoryId: Int64($0.category!), name: $0.name!, type: .disease, lang: lang) })!
                    }
                    DispatchQueue.main.async {
                        after(.done, results)
                    }
                    break
                case .failure(_):
                    DispatchQueue.main.async {
                        after(.noServerConnection, results)
                    }
                    break
            }
        }
        if isWritten{
            timestamp = Int(Date().timeIntervalSince1970.rounded())
            switch(lang){
            case "ru": UserDefaults.standard.set(timestamp, forKey: "ru.involta.diseasesRuTimestamp")
            case "en": UserDefaults.standard.set(timestamp, forKey: "ru.involta.diseasesEnTimestamp")
            case "es": UserDefaults.standard.set(timestamp, forKey: "ru.involta.diseasesEsTimestamp")
            default:   UserDefaults.standard.set(timestamp, forKey: "ru.involta.diseasesEnTimestamp")
            }
        }
    }
    
    static func diseases(id: Int, _ after: @escaping (ApiResult, [Item]) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            return
        }
        
        
        Alamofire.request("http://directorydiseases.ru/api6/disease/getDiseasesByCategoryId", method: .post, parameters: ["id": "\(id)", "localization": Localize.currentLanguage()]).validate().responseJSON { response in
            var results = [Item]()
            switch response.result {
                case .success(let json):
                    if let diseases = Diseases(json: json as! JSON) {
                        results = (diseases.diseases?.map { Item(id: Int64($0.id!), categoryId: Int64($0.category!), name: $0.name!, type: .disease, lang: Localize.currentLanguage()) })!
                    }
                    DispatchQueue.main.async {
                        after(.done, results)
                    }
                    break
                case .failure(_):
                    DispatchQueue.main.async {
                        after(.noServerConnection, results)
                    }
                    break
            }
        }
    }
    
    static func search(query: String, _ after: @escaping (ApiResult, [Disease], [Post]) -> Void) {

        if !Connectivity.isConnectedToInternet {
            return
        }
        
        Alamofire.request("http://directorydiseases.ru/api6/disease/search", method: .post, parameters: ["query": query, "localization": Localize.currentLanguage()]).validate().responseJSON { response in
            var p = [Post]()
            var d = [Disease]()
            switch response.result {
                case .success(let json):
                    if let posts = Posts(json: json as! JSON) {
                        if (posts.status! == 2) {
                            p = posts.posts!
                        }
                        if (posts.status! == 1) {
                            d = posts.diseases!
                        }
                    }
                    DispatchQueue.main.async {
                        after(.done, d, p)
                    }
                    break
            case .failure(_):
                DispatchQueue.main.async {
                    after(.noServerConnection, d, p)
                }
                break
            }

        }

    }
    
    static func content(ID: Int, lang: String, _ after: @escaping (ApiResult, [Content]) -> Void){
        
        if !Connectivity.isConnectedToInternet {
            return
        }
        
        Alamofire.request("http://directorydiseases.ru/api6/disease/getDiseaseById", method: .post, parameters: ["id": "\(ID)", "localization": lang]).validate().responseJSON { response in
            var results = [Content]()
            switch response.result {
            case .success(let json):
                if let d = ContentApi(json: json as! JSON) {
                    if (d.status! == 1 ) {
                        if((d.description?.count)! > 0){
                            results.append(Content(itemId: Int64(ID), header: "Description".localized(), data: d.description!, lang: lang))
                        }
                        if (d.symptoms?.count)! > 0{
                            results.append(Content( itemId: Int64(ID), header: "Symptoms".localized(), data: d.symptoms!, lang: lang))
                        }
                        for b in d.blocks! {
                            if((b.data?.count)! > 0){
                                results.append(Content(itemId: Int64(ID), header: b.header!, data: b.data!, lang: lang))
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    after(.done, results)
                }
                break;
            case .failure(_):
                DispatchQueue.main.async {
                    after(.noServerConnection, results)
                }
                break
            }
        }
    }
    
    static func saveItems(table: Table, items: [Item]){
        for i in items {
            try! DataBaseManager.shared.db.run(table.insert(or: OnConflict.ignore, Item.idField <- Int64(i.id!), Item.nameField <- i.name, Item.categoryIdField <- Int64(i.categoryId!), Item.typeField <- i.type.rawValue))
        }
    }
    
    static func saveCategories(table: Table, categories: [Category]){
        for c in  categories.filter({ $0.name != "WITHOUT CATEGORY" && $0.name != "Without category" }) {
            try! DataBaseManager.shared.db.run(table.insert(or: OnConflict.replace, Category.idField <- Int64(c.id!), Category.nameField <- c.name))
        }
    }
    static func saveContent(selectedId: Int64, lang: String, contents: [Content]){
        for c in contents {
            try! DataBaseManager.shared.db.run(Content.table.insert(
                or: OnConflict.ignore,
                Content.itemIdField <- selectedId,
                Content.headerField <- c.header,
                Content.dataField <- c.data,
                Content.languageField <- lang
            ))
        }
    }
    
}
