//
//  Content.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import SQLite
import Localize_Swift

class Content {
    
    let itemId: Int64
    let header: String
    let data: String
    let language: String
    
    init(itemId: Int64, header: String, data: String, lang: String) {
        self.itemId = itemId
        self.header = header
        self.data = data
        self.language = lang
    }
    
}

extension Content {
    
    static let table = Table("content")
    
    static let itemIdField = Expression<Int64>("item_id")
    static let headerField = Expression<String>("header")
    static let dataField = Expression<String>("data")
    static let languageField = Expression<String>("language")
    
    static func get(itemID: Int64, lang: String) -> [Content] {
        
        var results = [Content]()

        do {
            for row in try DataBaseManager.shared.db.prepare(table.filter(itemIdField == Int64(itemID) && languageField == lang)) {
                results.append(Content(
                    itemId: row[itemIdField],
                    header: row[headerField],
                    data: row[dataField],
                    lang: lang
                ))
            }
        } catch {
            print("Error in Content.get(itemId: Int)")
        }
        
        return results
        
    }
    
    static func get(lan: String) -> [Content] {
        
        var results = [Content]()
        
        
        do {
            for row in try DataBaseManager.shared.db.prepare(table.filter( languageField == lan ) ){
                results.append(Content(
                    itemId: row[itemIdField],
                    header: row[headerField],
                    data: row[dataField],
                    lang: lan
                ))
            }
        } catch {
            print("Error in Content.get(itemId: Int)")
        }
        
        return results
        
    }
    
}
