//
//  Item.swift
//  sprovochnikzobolevani
//
//  Created by Apple on 04.07.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import SQLite
import Localize_Swift

enum ItemType: Int {
    case procedure = 0
    case diet = 1
    case disease = 2
    func name()->(String,UIColor)?{
        switch self {
        case .diet:
            return ("Diets".localized(), UIColor(red: 138/255, green: 223/255, blue: 81/255, alpha: 1))
        case .disease:
            return ("Diseases".localized(), UIColor(red: 253/255, green: 67/255, blue: 56/255, alpha: 1))
        case.procedure:
            return ("Procedures".localized(), UIColor(red: 255/255, green: 217/255, blue: 97/255, alpha: 1))
        }
    }
}

class Item {
    
    let id: Int64?
    let categoryId: Int64?
    let name: String
    let type: ItemType
    let language: String
    
    lazy var content = Content.get(itemID: self.id!, lang: Localize.currentLanguage())
    
    lazy var category = Category.get(id: categoryId!, lang: Localize.currentLanguage())
    
    init(id: Int64, categoryId: Int64?, name: String, type: ItemType, lang: String) {
        self.id = id
        self.categoryId = categoryId
        self.name = name
        self.type = type
        self.language = lang
    }
    
}

extension Item {
    
    static let tableEn = Table("itemEn")
    static let tableRu = Table("itemRu")
    static let tableEs = Table("itemEs")
    
    static let idField = Expression<Int64>("id")
    static let categoryIdField = Expression<Int64?>("category_id")
    static let nameField = Expression<String>("name")
    static let typeField = Expression<Int>("type")
    
    static func get(lang: String) -> [Item] {
        
        var results = [Item]()
        var table: Table
        switch lang {
        case "ru" : table = tableRu
        case "en" : table = tableEn
        case "es" : table = tableEs
        default: table = tableEn
        }
        do {
            for row in try DataBaseManager.shared.db.prepare(table) {
                var a: Int64? = 0
                if let c = row[categoryIdField] {
                    a = Int64(c)
                }
                
                results.append(Item(
                    id: row[idField],
                    categoryId: a,
                    name: row[nameField],
                    type: ItemType(rawValue: Int(row[typeField]))!,
                    lang: lang
                ))
            }
        } catch {
            print("Error in Item.get()")
        }
        
        return results
        
    }
    
    static func get(type: ItemType, lang: String) -> [Item] {
        
        var results = [Item]()
        var table: Table
        switch lang {
        case "ru" : table = tableRu
        case "en" : table = tableEn
        case "es" : table = tableEs
        default: table = tableEn
        }
        do {
            for row in try DataBaseManager.shared.db.prepare(table.filter(typeField == type.rawValue)).sorted(by: { (firstRow, secondRow) -> Bool in
                return firstRow[nameField] < secondRow[nameField]
            }) {
                var a: Int64? = 0
                if let c = row[categoryIdField] {
                    a = Int64(c)
                }
                results.append(Item(
                    id: row[idField],
                    categoryId: a,
                    name: row[nameField],
                    type: ItemType(rawValue: Int(row[typeField]))!,
                    lang: lang
                ))
            }
        } catch {
            print("Error in Item.get()")
        }
        
        return results
        
    }
    
    static func get(categoryId: Int, lang: String) -> [Item] {
        
        var results = [Item]()
        var table: Table
        switch lang {
        case "ru" : table = tableRu
        case "en" : table = tableEn
        case "es" : table = tableEs
        default: table = tableEn
        }
        do {
            for row in try DataBaseManager.shared.db.prepare(table.filter(categoryIdField == Int64(categoryId) )).sorted(by: { (firstRow, secondRow) -> Bool in
                return firstRow[nameField] < secondRow[nameField]
            }) {
                var a: Int64? = 0
                if let c = row[categoryIdField] {
                    a = Int64(c)
                }
                results.append(Item(
                    id: row[idField],
                    categoryId: a,
                    name: row[nameField],
                    type: ItemType(rawValue: Int(row[typeField]))!,
                    lang: lang
                ))
            }
        } catch {
            print("Error in Item.get()")
        }
        
        return results
        
    }
    
    static func get(id: Int64, lang: String) -> Item? {
        
        var result: Item? = nil
        var table: Table
        switch lang {
        case "ru" : table = tableRu
        case "en" : table = tableEn
        case "es" : table = tableEs
        default: table = tableEn
        }
        do {
            let row = try DataBaseManager.shared.db.pluck(table.filter(idField == id))
                var a: Int64? = 0
                if let c = row![categoryIdField] {
                    a = Int64(c)
                }
                result = Item(
                    id: row![idField],
                    categoryId: a,
                    name: row![nameField],
                    type: ItemType(rawValue: Int(row![typeField]))!,
                    lang: lang
                )
            
        } catch {
            print("Error in Item.get()")
        }
        
        return result
        
    }
    
}

extension Item {
    var isSaved:Bool {
        return SavedObject.get(type: .bookmark).map({ (s) -> Item in
            return s.item()
        }).contains(where: { (r) -> Bool in
            r.id == self.id && r.name == self.name
        })
    }
}
