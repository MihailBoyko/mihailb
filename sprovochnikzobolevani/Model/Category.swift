//
//  Category.swift
//  sprovochnikzobolevani
//
//  Created by Apple on 04.07.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import SQLite
import Localize_Swift

class Category {
  
    let id: Int64?
    let name: String
    
    init(id: Int64, name: String) {
        self.id = id
        self.name = name
    }
    
}

extension Category {
    
    static let tableEn = Table("categoryEn")
    static let tableRu = Table("categoryRu")
    static let tableEs = Table("categoryEs")
    
    static let idField = Expression<Int64>("id")
    static let nameField = Expression<String>("name")
    
    static func get(lang: String) -> [Category] {
        
        var results = [Category]()
        var table: Table
        switch lang {
            case "ru" : table = tableRu
            case "en" : table = tableEn
            case "es" : table = tableEs
            default: table = tableEn
        }
        
        do {
            for row in try DataBaseManager.shared.db.prepare(table.filter(idField > 1)){
                results.append(Category(
                    id: row[idField],
                    name: row[nameField]
                ))
            }
        } catch {
            print("Error in Category.get(itemId: Int)")
        }
        
        return results
        
    }
    
    static func get(id: Int64, lang: String) -> Category? {
        
        var result: Category? = nil
        var table: Table
        switch lang {
        case "ru" : table = tableRu
        case "en" : table = tableEn
        case "es" : table = tableEs
        default: table = tableEn
        }
        do {
            let row = try DataBaseManager.shared.db.pluck(table.filter(idField == id))
            result = Category(
                id: row![idField],
                name: row![nameField]
            )
            
        } catch {
            print("Error in Category.get()")
        }
        
        return result
        
    }
    
}
