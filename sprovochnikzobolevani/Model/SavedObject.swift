//
//  SavedObject.swift
//  sprovochnikzobolevani
//
//  Created by Apple on 04.07.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import Localize_Swift

enum SavedObjectType: Int {
    case history = 0
    case bookmark = 1
}

class SavedObject: NSObject, NSCoding {
    
    var id: Int?
    var name: String?
    var itemType: ItemType?
    var type: SavedObjectType?
    var timestamp: Int?
    var language: String
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.itemType?.rawValue, forKey: "itemType")
        aCoder.encode(self.type?.rawValue, forKey: "type")
        aCoder.encode(self.timestamp, forKey: "timestamp")
        aCoder.encode(self.language, forKey: "language")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = (aDecoder.decodeObject(forKey: "id") as? Int)
        self.name = (aDecoder.decodeObject(forKey: "name") as? String)
        if let it = aDecoder.decodeObject(forKey: "itemType") as? Int {
            self.itemType = ItemType(rawValue: it)
        } else {
            self.itemType = nil
        }
        if let itt = aDecoder.decodeObject(forKey: "type") as? Int {
            self.type = SavedObjectType(rawValue: itt)
        } else {
            self.type = nil
        }
        self.timestamp = (aDecoder.decodeObject(forKey: "timestamp") as? Int)
        self.language = (aDecoder.decodeObject(forKey: "language") as? String)!
    }
    
    init(id: Int, name: String, itemType: ItemType, type: SavedObjectType, timestamp: Int, lang: String) {
        self.id = id
        self.name = name
        self.itemType = itemType
        self.type = type
        self.timestamp = timestamp
        self.language = lang
    }
    
    func item() -> Item {
        return Item(id: Int64(id!), categoryId: nil, name: name!, type: itemType!, lang: language)
    }
    
    static func save(item: Item, type: SavedObjectType) {
        
        let savedObject = SavedObject(id: Int(item.id!), name: item.name, itemType: item.type, type: type, timestamp: Int(Date().timeIntervalSince1970.rounded()), lang: item.language)
        
        guard
            let data = UserDefaults.standard.data(forKey: "ru.involta.saved.objects.\(type.rawValue)"),
            var savedObjects = NSKeyedUnarchiver.unarchiveObject(with: data) as? [SavedObject] else {
                saveObjects(objects: [savedObject], type: type)
                return
        }
        
        let isSaved = savedObjects.contains(where: {(s) -> Bool in s.id == savedObject.id})
        
        if !isSaved {
            savedObjects.append(savedObject)
            saveObjects(objects: savedObjects, type: type)
        }
        
    }
    
    static func get(type: SavedObjectType) -> [SavedObject]{
        guard let data = UserDefaults.standard.data(forKey: "ru.involta.saved.objects.\(type.rawValue)") else {
            return [SavedObject]()
        }
        
        let objects = (NSKeyedUnarchiver.unarchiveObject(with: data) as? [SavedObject])!
        
        if (type == .history) {
            let edited = objects.filter { (Int(Date().timeIntervalSince1970.rounded()) - $0.timestamp!) < 604800 }
            saveObjects(objects: edited, type: type)
            return edited
        }
        
        return objects
        
    }
    
    static func delete(type: SavedObjectType) {
        saveObjects(objects: [], type: type)
    }
    
    static func remove(id: Int, type: SavedObjectType) {
        guard
            let data = UserDefaults.standard.data(forKey: "ru.involta.saved.objects.\(type.rawValue)"),
            var savedObjects = NSKeyedUnarchiver.unarchiveObject(with: data) as? [SavedObject],
            let index = savedObjects.index(where: { (s) -> Bool in s.id == id }) else {
                return
        }
        
        savedObjects.remove(at: index)
        saveObjects(objects: savedObjects, type: type)
    }
    
    private static func saveObjects(objects: [SavedObject], type: SavedObjectType) {
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: objects), forKey: "ru.involta.saved.objects.\(type.rawValue)")
        UserDefaults.standard.synchronize()
    }
    
}
