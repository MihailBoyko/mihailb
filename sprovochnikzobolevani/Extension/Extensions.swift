//
//  Extensions.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Localize_Swift
import NVActivityIndicatorView
import MYTableViewIndex

var isWritten = true

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
extension Array {
    /// Picks `n` random elements (partial Fisher-Yates shuffle approach)
    func randomget (randomPick n: Int) -> [Element] {
        var copy = self
        for i in stride(from: count - 1, to: count - n - 1, by: -1) {
            copy.swapAt(i, Int(arc4random_uniform(UInt32(i + 1))))
        }
        return Array(copy.suffix(n))
    }
}

extension UINavigationController {
    
    func addSearchController(items: [Item]) {
        
        self.navigationBar.prefersLargeTitles = true
        
        let searchResultVc = storyboard?.instantiateViewController(withIdentifier: storyBoardIdentifier.searchTableViewResult) as? SearchTableViewController
        
        let searchController = UISearchController(searchResultsController: searchResultVc)
        searchResultVc?.searchBar = searchController.searchBar
        searchResultVc?._searchViewModel = SearchViewModel(items: items)
        searchController.searchResultsController?.view.isHidden = false
        
        for subView in (searchResultVc?.searchBar.subviews)!  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    var bounds: CGRect
                    bounds = textField.frame
                    textField.bounds = bounds
                    textField.layer.cornerRadius = 10
                    textField.layer.borderWidth = 2.0
                    textField.layer.borderColor = UIColor(red:0.17, green:0.48, blue:1.00, alpha:1.0).cgColor
                    textField.backgroundColor = UIColor.white
                }
            }
        }
        
        
        let topViewController = UIApplication.topViewController()
        topViewController?.definesPresentationContext = true
        
        topViewController?.navigationItem.searchController = searchController
        //topViewController?.navigationItem.searchController?.delegate = self
        
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action:#selector(setSearchBar))
        
        topViewController?.navigationItem.rightBarButtonItem = searchButton
        
    }
    @objc func setSearchBar(){
        topViewController?.navigationItem.searchController?.searchBar.becomeFirstResponder()

    }
  
    
}
extension UIImage {
    
    static func getFromRessourceWith(name:String)-> UIImage!{
        let firstIndex = name.index(name.startIndex, offsetBy:3)
        let endIndex = name.index(name.endIndex, offsetBy: -4)
        let new_path = String(name[firstIndex..<endIndex])
        
        if let ressource = Bundle.main.path(forResource: new_path, ofType: ".jpg", inDirectory: "Content_images"){
            return UIImage(contentsOfFile: ressource)
        }
        return nil
    }
}

struct storyBoardIdentifier {
    static let searchTableViewResult = "searchTableViewResult"
}


extension UIViewController {
    
    func noNetworkConnectionAlert(_ controller: UIViewController, _ after: (() -> Void)?) {
        
        let alert = UIAlertController(title: "", message: "No network connection!".localized(), preferredStyle: .alert)
        
        let repeatAction = UIAlertAction(title: "Repeat".localized(), style: UIAlertActionStyle.default) {
            UIAlertAction in
            controller.dismiss(animated: true)
            if let after = after {
                after()
            }
        }
        let backAction = UIAlertAction(title: "Back".localized(), style: UIAlertActionStyle.default) {
            UIAlertAction in
            controller.dismiss(animated: true)
            controller.navigationController?.popViewController(animated: true)
        }
        
        alert.addAction(repeatAction)
        alert.addAction(backAction)
        
        controller.present(alert, animated: true)
        
    }
    
    func noServerConnectionAlert(_ controller: UIViewController) {
        
        let alert = UIAlertController(title: "", message: "No server connection!".localized(), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Ok".localized(), style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(cancelAction)
        
        controller.present(alert, animated: true)
        
    }
    
    func errorAlert(_ controller: UIViewController) {
        
        let alert = UIAlertController(title: nil, message: "An error occurred while connecting to the database".localized(), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Ok".localized(), style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(cancelAction)
        
        controller.present(alert, animated: true)
        
    }
    
    func loadingAlert(_ controller: UIViewController) {

//        let alert = UIAlertController(title: nil, message: "Loading".localized(), preferredStyle: .alert)
//
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
//
//        alert.view.addSubview(loadingIndicator)
//
//        loadingIndicator.startAnimating()
//
//        controller.present(alert, animated: true)

    }
    
    func switchLanguageAlert(_ controller: UIViewController, _ after: (() -> Void)?) {
        
        let alert = UIAlertController(title: nil, message: "Switch language".localized(), preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        
        for language in Localize.availableLanguages() {
            if (language == "ru" || language == "en" || language == "es") {
                let displayName = Localize.displayNameForLanguage(language)
                let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    if (Localize.currentLanguage() != language) {
                        Localize.setCurrentLanguage(language)
                        if let after = after {
                            after()
                        }
                    }
                })
                alert.addAction(languageAction)
            }
        }
        
        alert.addAction(cancelAction)
        
        controller.present(alert, animated: true)
        
    }
    
    func after(_ result: ApiResult, _ after: @escaping () -> Void) {
        
        switch result {
        case .databaseError:
            self.errorAlert(self)
            break
        case .noServerConnection:
            if Connectivity.isConnectedToInternet {
                //self.noServerConnectionAlert(self)
            }
            break
        case .done:
            after()
            break;
        }
        
    }
    
    func write(_ after: @escaping () throws -> Void) {
        
        if isWritten {
            isWritten = false
            
            DispatchQueue.global(qos: .background).async {
            
                do {
                    try after()
                } catch _ {
                    self.errorAlert(self)
                }
                
                DispatchQueue.main.async {
                    isWritten = true
                }
                
            }
        }        
    }
    
}

