//
//  TableDataSources.swift
//  MYTableViewIndex
//
//  Created by Makarov Yury on 09/07/16.
//  Copyright © 2016 Makarov Yury. All rights reserved.
//
import UIKit
import MYTableViewIndex

protocol ItemData {}

protocol DataSource {
    
    func numberOfSections() -> Int
    
    func numberOfItemsInSection(_ section: Int) -> Int
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> ItemData?
    
    func titleForHeaderInSection(_ section: Int) -> String?
}

extension String : ItemData {}

struct CountryDataSource : DataSource {
    
    fileprivate(set) var sections = [[String]]()
    
    fileprivate let collaction = UILocalizedIndexedCollation.current()
    
    init() {
        sections = split(loadCountryNames())
    }
    
    fileprivate func loadCountryNames() -> [String] {
        return Locale.isoRegionCodes.map { (code) -> String in
            return Locale.current.localizedString(forRegionCode: code)!
        }
    }
    
    fileprivate func split(_ items: [String]) -> [[String]] {
        let collation = UILocalizedIndexedCollation.current()
        let items = collation.sortedArray(from: items, collationStringSelector: #selector(NSObject.description)) as! [String]
        var sections = [[String]](repeating: [], count: collation.sectionTitles.count)
        for item in items {
            let index = collation.section(for: item, collationStringSelector: #selector(NSObject.description))
            sections[index].append(item)
        }
        return sections
    }
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func numberOfItemsInSection(_ section: Int) -> Int {
        return sections[section].count
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> ItemData? {
        return sections[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).item]
    }
    
    func titleForHeaderInSection(_ section: Int) -> String? {
        return collaction.sectionTitles[section]
    }
}

extension UIColor : ItemData {}

struct CompoundDataSource : DataSource {
    
    fileprivate let colorsSection = [UIColor.lightGray, UIColor.gray, UIColor.darkGray]
    
    fileprivate let countryDataSource = CountryDataSource()
    
    func numberOfSections() -> Int {
        return countryDataSource.numberOfSections() + 1
    }
    
    func numberOfItemsInSection(_ section: Int) -> Int {
        return section == 0 ? colorsSection.count : countryDataSource.numberOfItemsInSection(section - 1)
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> ItemData? {
        if (indexPath as NSIndexPath).section == 0 {
            return colorsSection[(indexPath as NSIndexPath).item]
        } else {
            return countryDataSource.itemAtIndexPath(IndexPath(item: (indexPath as NSIndexPath).item, section: (indexPath as NSIndexPath).section - 1))
        }
    }
    
    func titleForHeaderInSection(_ section: Int) -> String? {
        return section == 0 ? nil : countryDataSource.titleForHeaderInSection(section - 1)
    }
}

enum ExampleType : String {
    case comparison
    case customBackground
    case hidingIndex
    case coloredIndex
    case imageIndex
    case largeFont
    case collectionView
}

protocol Example {
    
    var dataSource: DataSource { get }
    
    var indexDataSource: TableViewIndexDataSource { get }
    
    var hasSearchIndex: Bool { get }
    
    func mapIndexItemToSection(_ indexItem: IndexItem, index: NSInteger) -> Int
    
    func setupTableIndexController(_ tableIndexController: TableViewIndexController) -> Void
    
    func trackSelectedSections(_ sections: Set<Int>)
}

class BasicExample : Example {
    
    var dataSource: DataSource {
        return CountryDataSource()
    }
    
    var indexDataSource: TableViewIndexDataSource {
        return CollationIndexDataSource(hasSearchIndex: hasSearchIndex)
    }
    
    var hasSearchIndex: Bool {
        return false
    }
    
    func mapIndexItemToSection(_ indexItem: IndexItem, index: NSInteger) -> Int {
        return hasSearchIndex ? index - 1 : index
    }
    
    func setupTableIndexController(_ tableIndexController: TableViewIndexController) -> Void {
        tableIndexController.tableViewIndex.dataSource = indexDataSource
    }
    
    func trackSelectedSections(_ sections: Set<Int>) {}
}

class SearchExample : BasicExample {
    
    override var hasSearchIndex: Bool {
        return true
    }
}

class BackgroundView : UIView {
    
    enum Alpha : CGFloat {
        case normal = 0.3
        case highlighted = 0.6
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 6
        layer.masksToBounds = false
        backgroundColor = UIColor.lightGray.withAlphaComponent(Alpha.normal.rawValue)
        isUserInteractionEnabled = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func pressed(_ sender: TableViewIndex) {
        backgroundColor = UIColor.lightGray.withAlphaComponent(Alpha.highlighted.rawValue)
    }
    
    @objc func released(_ sender: TableViewIndex) {
        UIView.animate(withDuration: 0.15) {
            self.backgroundColor = UIColor.lightGray.withAlphaComponent(Alpha.normal.rawValue)
        }
    }
}

class CustomBackgroundExample : BasicExample {
    
    fileprivate(set) var tableViewIndex: TableViewIndex!
    
    override func setupTableIndexController(_ tableIndexController: TableViewIndexController) {
        super.setupTableIndexController(tableIndexController)
        
        tableIndexController.tableViewIndex.font = UIFont.boldSystemFont(ofSize: 12.0)
        
        let backgroundView = BackgroundView()
        
        tableIndexController.tableViewIndex.backgroundView = backgroundView
        tableIndexController.tableViewIndex.indexInset = UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
        tableIndexController.tableViewIndex.indexOffset = UIOffset()
        
        tableIndexController.layouter = { tableView, tableIndex in
            var frame = tableIndex.frame
            if (UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft) {
                frame.origin = CGPoint(x: frame.origin.x + 3, y: frame.origin.y)
            } else {
                frame.origin = CGPoint(x: frame.origin.x - 3, y: frame.origin.y)
            }
            tableIndex.frame = frame
        };
        
        tableIndexController.tableViewIndex.addTarget(backgroundView, action: #selector(BackgroundView.pressed(_:)),
                                                      for: .touchDown)
        
        tableIndexController.tableViewIndex.addTarget(backgroundView, action: #selector(BackgroundView.released(_:)),
                                                      for: [.touchUpInside, .touchUpOutside])
        
        tableViewIndex = tableIndexController.tableViewIndex
    }
    
    override func trackSelectedSections(_ sections: Set<Int>) {
        let sortedSections = sections.sorted()
        
        UIView.animate(withDuration: 0.25, animations: {
            for (index, item) in self.tableViewIndex.items.enumerated() {
                let section = self.mapIndexItemToSection(item, index: index)
                let shouldHighlight = sortedSections.count > 0 && section >= sortedSections.first! && section <= sortedSections.last!
                
                item.tintColor = shouldHighlight ? UIColor.red : nil
            }
        })
    }
}

class LargeFontExample : SearchExample {
    
    override func setupTableIndexController(_ tableIndexController: TableViewIndexController) {
        super.setupTableIndexController(tableIndexController)
        
        tableIndexController.tableViewIndex.font = UIFont.boldSystemFont(ofSize: 15.0)
    }
}

class AutohidingIndexExample : BasicExample {
    
    override var dataSource: DataSource {
        return CompoundDataSource()
    }
    
    override var hasSearchIndex: Bool {
        return false
    }
}

class ColoredIndexExample : SearchExample {
    
    override var indexDataSource: TableViewIndexDataSource {
        return ColoredIndexDataSource()
    }
}

class ImageIndexExample : BasicExample {
    
    override var indexDataSource: TableViewIndexDataSource {
        return ImageIndexDataSource()
    }
    
    override func mapIndexItemToSection(_ indexItem: IndexItem, index: NSInteger) -> Int {
        return index <= 1 ? NSNotFound : index - 2
    }
}

func exampleByType(_ type: ExampleType) -> Example {
    switch type {
    case .customBackground:
        return CustomBackgroundExample()
    case .hidingIndex:
        return AutohidingIndexExample()
    case .coloredIndex:
        return ColoredIndexExample()
    case .imageIndex:
        return ImageIndexExample()
    case .largeFont:
        return LargeFontExample()
    case .comparison:
        return SearchExample()
    case .collectionView:
        return BasicExample()
    }
}

class CollationIndexDataSource : NSObject, TableViewIndexDataSource {
    
    fileprivate let collaction = UILocalizedIndexedCollation.current()
    
    fileprivate let showsSearchItem: Bool
    
    init(hasSearchIndex: Bool) {
        showsSearchItem = hasSearchIndex
    }
    
    convenience override init() {
        self.init(hasSearchIndex: true)
    }
    
    func indexItems(for tableViewIndex: TableViewIndex) -> [UIView] {
        var items = collaction.sectionIndexTitles.map{ title -> UIView in
            return StringItem(text: title)
        }
        if showsSearchItem {
            items.insert(SearchItem(), at: 0)
        }
        return items
    }
}


private func generateRandomNumber(from: UInt32, to: UInt32) -> UInt32 {
    return from + arc4random_uniform(to - from + 1)
}

extension UIColor {
    
    func my_shiftHue(_ shift: CGFloat) -> UIColor? {
        var hue: CGFloat = 0.0, saturation: CGFloat = 0.0, brightness: CGFloat = 0.0, alpha: CGFloat = 0.0
        if !getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            return nil
        }
        return UIColor(hue: (hue + shift).truncatingRemainder(dividingBy: 1.0), saturation: saturation, brightness: brightness, alpha: alpha)
    }
    
    func my_shiftHueRandomlyWithGradation(_ gradation: Int) -> UIColor {
        let rand = generateRandomNumber(from: 1, to: UInt32(gradation))
        let hueShift: CGFloat = 1.0 / CGFloat(rand)
        if let c = my_shiftHue(hueShift) {
            return c
        }
        return self
    }
}

class ColoredIndexDataSource : CollationIndexDataSource {
    
    override func indexItems(for tableViewIndex: TableViewIndex) -> [UIView] {
        var color = UIColor.red
        let gradation = collaction.sectionIndexTitles.count + 1
        
        var items = collaction.sectionIndexTitles.map{ title -> UIView in
            color = color.my_shiftHueRandomlyWithGradation(gradation)
            let item = StringItem(text: title)
            item.tintColor = color
            return item
        }
        let searchItem = SearchItem()
        searchItem.tintColor = color.my_shiftHueRandomlyWithGradation(gradation)
        items.insert(searchItem, at: 0)
        return items
    }
}

class ImageIndexDataSource : CollationIndexDataSource {
    
    convenience init() {
        self.init(hasSearchIndex: false)
    }
    
    override func indexItems(for tableViewIndex: TableViewIndex) -> [UIView] {
        var items = collaction.sectionIndexTitles.map{ title -> UIView in
            return StringItem(text: title)
        }
        let item1 = ImageItem(image: UIImage(named: "Contacts")!)
        item1.contentInset = UIEdgeInsets(top: 0, left: 0.5, bottom: -4, right: 0.5)
        items.insert(item1, at: 0)
        
        let item2 = ImageItem(image: UIImage(named: "Settings")!)
        item2.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -4, right: 0)
        items.insert(item2, at: 0)
        
        return items
    }
}
