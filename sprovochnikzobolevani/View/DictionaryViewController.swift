//
//  DictionaryViewController.swift
//  sprovochnikzobolevani
//
//  Created by Apple on 16.07.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import SQLite
import Localize_Swift
import NVActivityIndicatorView
import MYTableViewIndex

class DictionaryViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: MySearchBar!
    
    var viewModel = DictionaryViewModel()
    var searchViewModel = SearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Diseases".localized()
        //self.navigationController?.navigationBar.topItem?.backBarButtonItem?.title = "Directory".localized()
        
        
        // search
        searchBar.backgroundImage = UIImage()
        searchBar.backgroundColor = UIColor.white
        searchBar.barTintColor = UIColor.white
        searchBar.placeholder = "Search for diseases".localized()
        
        searchBar.customSearchBarDelagate = self
        
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib.init(nibName: "RecipCellTableViewCell", bundle: nil), forCellReuseIdentifier: "defaultCell")
        tableView.rowHeight = UITableViewAutomaticDimension
       // tableView.estimatedRowHeight = 300
        
        // clear cell's left margin
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        let tableViewIndexController = TableViewIndexController(scrollView: tableView)
        
        setupData()
       // self.navigationController?.addSearchController(items: self.viewModel.items)
        self.searchViewModel.items = self.viewModel.items.filter { $0.type == .disease }
        self.searchBar.setDatas(data: self.searchViewModel.items)
    }
    
    func setupData() {
        
        if isWritten {
            if viewModel.items.count == 0 {
                viewModel.items = Item.get(type: .disease, lang: Localize.currentLanguage())
//                if viewModel.items.count > 0 {
//                    self.navigationController?.addSearchController(items: self.viewModel.items)
//                }
            }
        }
        if !Connectivity.isConnectedToInternet && (viewModel.categories.count == 0 || viewModel.items.count == 0) {
            noNetworkConnectionAlert(self) {
                self.setupData()
            } 
        } else {
            if viewModel.categories.count == 0 {
                DiseaseServices.categories { r1, categories in self.after(r1) {
                    self.viewModel.categories = categories.filter { $0.name != "WITHOUT CATEGORY" && $0.name != "Without category" }
                    self.tableView.reloadData()
                }}
            }
        }
    }
    
}

extension DictionaryViewController : UITableViewDataSource, UITableViewDelegate {
    
    @objc func addToArchive(_ button:UIButton){
        let selected = viewModel.items.filter { $0.id! == button.tag }.first!
        DiseaseServices.content(ID: Int(selected.id!), lang: selected.language) { r, contents in self.after(r) {
            if Content.get(itemID: selected.id!, lang: Localize.currentLanguage()).count == 0 {
                self.write {
                    DiseaseServices.saveContent(selectedId: selected.id!, lang: selected.language, contents: contents)
                }
            }

           // self.navigationController?.addSearchController(items: self.viewModel.items)
            
            if selected.isSaved {
                SavedObject.remove(id: Int(selected.id!), type: .bookmark)
            } else {
                SavedObject.save(item: selected, type: .bookmark)
            }
            
            self.tableView.reloadData()
            
        }}
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.filter { $0.categoryId! == viewModel.categories[section].id }.count
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return viewModel.categories.map { $0.name }
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        if title == UITableViewIndexSearch {
            tableView.scrollRectToVisible((self.navigationItem.searchController?.searchBar.frame)!, animated: false)
            return NSNotFound
        } else {
            let sectionIndex = index
            return UILocalizedIndexedCollation.current().section(forSectionIndexTitle: sectionIndex)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.categories[section].name
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = tableView.frame

        let title = UILabel()
        title.frame =  CGRect(x: 15, y: 5, width: headerFrame.size.width - 30, height: 20)
        title.font = title.font.withSize(14.0)
        title.text = self.tableView(tableView, titleForHeaderInSection: section)
        title.textColor = UIColor.darkGray

        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: headerFrame.size.width, height: headerFrame.size.height))
        headerView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
        headerView.addSubview(title)

        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell", for: indexPath) as? RecipCellTableViewCell
        cell?.referenceNameLabel.text = viewModel.items.filter { $0.categoryId! == viewModel.categories[indexPath.section].id }[indexPath.item].name
        cell?.favoritBts.tag = Int(viewModel.items.filter { $0.categoryId! == viewModel.categories[indexPath.section].id! }[indexPath.item].id!)
        cell?.favoritBts.addTarget(self, action: #selector(addToArchive(_:)), for: .touchUpInside)
        cell?.favoritBts.setImage(viewModel.items.filter { $0.categoryId! == viewModel.categories[indexPath.section].id }[indexPath.item].isSaved ? #imageLiteral(resourceName: "favorite_selected") : #imageLiteral(resourceName: "favorite_normal"), for: .normal)
        
        // clear cell's left margin
        cell?.layoutMargins = UIEdgeInsets.zero
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let itemscrenVC = storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
            let selected = viewModel.items.filter { $0.categoryId! == viewModel.categories[indexPath.section].id! }[indexPath.item]
            itemscrenVC.viewModel = ItemScreenViewModel(id: Int(viewModel.items.filter { $0.categoryId! == viewModel.categories[indexPath.section].id! }[indexPath.item].id!))
            itemscrenVC.viewModel._reference = selected
            viewModel.navigateTo(itemscrenVC, .push)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension DictionaryViewController: CustomSearchBarDelagate{
    
    func onClickItemSuggestionsView(item: Item) {
        if  let itemscrenVC = storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
            itemscrenVC.viewModel = ItemScreenViewModel(id: Int(item.id!))
            itemscrenVC.viewModel._reference = item
            viewModel.navigateTo(itemscrenVC, .push)
            
        }
    }
    
    func onClickPostSuggestionsView(post: Post) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: post.permalink!)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: post.permalink!)!)
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func onClickShadowView(shadowView: UIView) {
        print("User touched shadowView")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Text did change, what i'm suppose to do ?")
    }
    
}
