//
//  ItemScreenTableViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 18.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SQLite
import Localize_Swift

class ItemScreenTableViewController: UITableViewController {
   
    var viewModel:ItemScreenViewModel!
    
    var disposed = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsSelection = false
        self.navigationController?.navigationBar.topItem?.backBarButtonItem?.title = "Back".localized()
        self.navigationItem.title = viewModel._reference.name
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500
        
        addFavorit()
        
        self.navigationItem.rightBarButtonItem?.image = (self.viewModel._reference.isSaved) ? UIImage(named: "favorite_selected")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal):#imageLiteral(resourceName: "favorite_normal")

        setupData()
    }
    
    func setupData() {
        
        if isWritten{
            viewModel._reference.content = Content.get(itemID: viewModel._reference.id!, lang: viewModel._reference.language)
        }
        
        if !Connectivity.isConnectedToInternet && viewModel._reference.content.count == 0 { 
            noNetworkConnectionAlert(self) {
                self.setupData()
            }
        } else if (viewModel._reference.content.count == 0) {
            if ( viewModel._reference.content.count == 0 ){
                if(self.viewModel._reference.type == .disease){
                    DiseaseServices.content(ID: Int(self.viewModel._reference.id!), lang: self.viewModel._reference.language) { r, contents in
                        self.after(r){
                            self.viewModel._reference.content = contents
                            self.write {
                                for c in contents {
                                    try DataBaseManager.shared.db.run(Content.table.insert(
                                        Content.itemIdField <- self.viewModel._reference.id!,
                                        Content.headerField <- c.header,
                                        Content.dataField <- c.data,
                                        Content.languageField <- c.language
                                    ))
                                }
                            }
                            self.tableView.reloadData()
                            self.viewModel?.saveInHystory()
                        }
                    }
                }else if(self.viewModel._reference.type == .procedure) {
                    ProcedureServices.content(ID: Int(self.viewModel._reference.id!), lang: self.viewModel._reference.language) { r, contents in
                        self.after(r){
                            self.viewModel._reference.content = contents
                            self.write {
                                for c in contents {
                                    try DataBaseManager.shared.db.run(Content.table.insert(
                                        Content.itemIdField <- self.viewModel._reference.id!,
                                        Content.headerField <- c.header,
                                        Content.dataField <- c.data,
                                        Content.languageField <- c.language
                                    ))
                                }
                            }
                            self.tableView.reloadData()
                            self.viewModel?.saveInHystory()
                        }
                    }
                }
            }
        }else{
            viewModel?.saveInHystory()
        }
        
    }

    func addFavorit(){
        let favoritButton = UIBarButtonItem(image: #imageLiteral(resourceName: "favorite_normal"), style: .done, target: self, action: nil)
        
        favoritButton.rx.tap.subscribe({ (_) in
            (self.viewModel._reference.isSaved) ? SavedObject.remove(id: Int(self.viewModel._reference.id!), type: .bookmark) : SavedObject.save(item: self.viewModel._reference, type: .bookmark)
            favoritButton.image = (self.viewModel._reference.isSaved) ? UIImage(named: "favorite_selected")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal):#imageLiteral(resourceName: "favorite_normal")            
        }).disposed(by: self.disposed)
        
        self.navigationItem.rightBarButtonItem = favoritButton
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberofContentSection()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let hearder = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 100))
        let label = UILabel(frame: CGRect(x: 16, y: -40, width: tableView.bounds.width - 16, height: 80))
        label.text = viewModel._reference.content[section].header
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        label.adjustsFontForContentSizeCategory = true

        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor(red: 66/255, green: 133/255, blue: 244/255, alpha: 1)

        hearder.addSubview(label)
        hearder.addConstraint(NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: NSLayoutRelation.equal, toItem: hearder, attribute: .centerY, multiplier: 1, constant: 0))
        return section > 0 ? hearder : nil
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = viewModel._reference.content[indexPath.row + indexPath.section].data
        cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
        cell.textLabel?.numberOfLines = 0
        cell.isUserInteractionEnabled = true
        return cell
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat{
        return section > 0 ? 80 : 0.01
     }
}
