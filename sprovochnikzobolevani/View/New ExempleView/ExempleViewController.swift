//
//  ExempleViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 29.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import MarqueeLabel
import Alamofire
import Gloss
import SQLite

class ExempleViewController: UIViewController {
    
    var viewModel = ShuffleViewModel()
    
    var shuffled = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()


        setText()

    }
    
    func  ConfigureView(){
        populateCardView()
    }
    
    
    func getAllCardView()-> [CardVIew] {
        var cardBag:[CardVIew] = [CardVIew]()
        
        for v in self.view.subviews{
            if v is CardVIew{
                cardBag.append(v as! CardVIew)
                v.updateConstraintsIfNeeded()
                v.setNeedsLayout()
            }
        }
        return cardBag
    }
    
    func setText(){
        let cards = getAllCardView()
        for (index,v) in cards.enumerated(){
            for t in v.subviews{
                if t is MarqueeLabel{
                    let titleLabel  = (t as! MarqueeLabel)
                    titleLabel.tag = index
                    titleLabel.text = titleLabel.text?.localized()
                    if titleLabel.bounds.width >= v.bounds.width{
                        titleLabel.marqueeType = .MLContinuous
                        titleLabel.scrollDuration = CGFloat((titleLabel.text?.count)! * 3)
                        titleLabel.animationCurve = .curveLinear
                    }
                    titleLabel.preferredMaxLayoutWidth = 30
                    titleLabel.adjustsFontSizeToFitWidth = true
                }
                
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(goToItemDetail))
            
            v.tag = index
            v.addGestureRecognizer(tap)
        }
    }
    
    func unShuffl() {
        let defStrings = ["Leukemia in children",
            "Diet for osteochondrosis" ,
            "Folliculometry",
            "Allergy"]
        let cards = getAllCardView()
        for (index,v) in cards.enumerated(){
            for t in v.subviews{
                if t is MarqueeLabel{
                    let titleLabel  = (t as! MarqueeLabel)
                    titleLabel.tag = index
                    titleLabel.text = defStrings[index].localized()
                    if titleLabel.bounds.width >= v.bounds.width{
                        titleLabel.marqueeType = .MLContinuous
                        titleLabel.scrollDuration = CGFloat((titleLabel.text?.count)! * 3)
                        titleLabel.animationCurve = .curveLinear
                    }
                    titleLabel.preferredMaxLayoutWidth = 30
                    titleLabel.adjustsFontSizeToFitWidth = true
                }
                
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(goToItemDetail))
            
            v.tag = index
            v.addGestureRecognizer(tap)
        }
    }
    
    func populateCardView(){
        let cards = getAllCardView()
        for (index,v) in cards.enumerated(){
            for t in v.subviews{
                if t is MarqueeLabel{
                 let titleLabel  = (t as! MarqueeLabel)
                    titleLabel.tag = index
                    titleLabel.text = " " + (shuffled[index].name)
                    if titleLabel.bounds.width >= v.bounds.width{
                    titleLabel.marqueeType = .MLContinuous
                        titleLabel.scrollDuration = CGFloat((titleLabel.text?.count)! * 3)
                    titleLabel.animationCurve = .curveLinear
//                    titleLabel.fadeLength = 10.0
//                    titleLabel.leadingBuffer = 10.0
//                    titleLabel.trailingBuffer = 10.0
                    }
                    titleLabel.preferredMaxLayoutWidth = 30
                    titleLabel.adjustsFontSizeToFitWidth = true
                   
//                    if (titleLabel.text?.count)! <= 10 {
//                         v.sizeToFit()
//                    }
                }
              
            }

            let tap = UITapGestureRecognizer(target: self, action: #selector(goToItemDetail))
            
            v.tag = index
            v.addGestureRecognizer(tap)
        }
        
    }
    
    @objc func goToItemDetail(_ gesture:UITapGestureRecognizer){
        if shuffled.count == 0 {
            return
        }
        let index = gesture.view?.tag

            if  let itemscrenVC = storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
                let selected = shuffled[index!]
                itemscrenVC.viewModel = ItemScreenViewModel()
                itemscrenVC.viewModel._reference = selected
                viewModel.navigateTo(itemscrenVC, .push)
            }
        
    }
    
    @IBAction func suffle (){
        if Connectivity.isConnectedToInternet || viewModel.items.count >= 4 {
            shuffled = viewModel.items.randomget(randomPick: 4)
            ConfigureView()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
