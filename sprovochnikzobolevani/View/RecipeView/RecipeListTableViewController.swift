//
//  RecipeListTableViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 17.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import SQLite
import Localize_Swift
import NVActivityIndicatorView

class RecipeListTableViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var searchBar: MySearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel = ListViewModel()
    
    var searchViewModel = SearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // search
        searchBar.backgroundImage = UIImage()
        searchBar.backgroundColor = UIColor.white
        searchBar.barTintColor = UIColor.white
        
        searchBar.customSearchBarDelagate = self
        
        switch viewModel.type {
        case .diet:
            searchBar.placeholder = "Search by diet".localized()
        case .procedure:
            searchBar.placeholder = "Search by procedures".localized()
        case .disease:
            searchBar.placeholder = "Search for diseases".localized()
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.layer.borderWidth = 1
        tableView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
        tableView.rowHeight = 60
        
        tableView.register(UINib.init(nibName: "RecipCellTableViewCell", bundle: nil), forCellReuseIdentifier: "defaultCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        //self.navigationController?.navigationBar.topItem?.backBarButtonItem?.title = "Back".localized()
        
        // clear cell's left margin
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        
        tableView.tableFooterView = UIView()
        
        // clear table's top margin
       // tableView.contentInset = UIEdgeInsetsMake(-20, 0, -20, 0)
        
       // self.navigationController?.addSearchController(items: viewModel.items)
        
        setupData()
        self.searchViewModel.items = self.viewModel.items
        self.searchBar.setDatas(data: self.searchViewModel.items)
    }
    
    func setupData() {
        
       
        if viewModel.items.count >= 0 {
            viewModel.items.append(contentsOf: Item.get(type: viewModel.type, lang: Localize.currentLanguage()))
        }
       
        if !Connectivity.isConnectedToInternet && viewModel.items.count == 0 {
            noNetworkConnectionAlert(self) {
                self.setupData()
            }
            
        }
       // self.navigationController?.addSearchController(items: viewModel.items)
        self.tableView.reloadData()
    }
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        tableView.reloadData()
    }
    @objc func addToArchive(_ button:UIButton){
        let selectedreference = viewModel.items[button.tag]
        DiseaseServices.content(ID: Int(selectedreference.id!), lang: selectedreference.language) { r, contents in
            for c in contents {
                try! DataBaseManager.shared.db.run(Content.table.insert(
                    Content.itemIdField <- Int64(selectedreference.id!),
                    Content.headerField <- c.header,
                    Content.dataField <- c.data,
                    Content.languageField <- Localize.currentLanguage()
                ))
            }
            (selectedreference.isSaved) ? SavedObject.remove(id: Int(selectedreference.id!), type: .bookmark) :
                SavedObject.save(item: selectedreference, type: .bookmark)
            self.tableView.reloadData()
        }
    }

}

extension RecipeListTableViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell", for: indexPath) as? RecipCellTableViewCell
        cell?.referenceNameLabel.text = viewModel.items[indexPath.item].name
        cell?.favoritBts.tag = indexPath.item
        cell?.favoritBts.addTarget(self, action: #selector(addToArchive(_:)), for: .touchUpInside)
        cell?.favoritBts.setImage(viewModel.items[indexPath.item].isSaved ? #imageLiteral(resourceName: "favorite_selected") : #imageLiteral(resourceName: "favorite_normal"), for: .normal)
        
        // clear cell's left margin
        cell?.layoutMargins = UIEdgeInsets.zero
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let itemscrenVC = storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
                let selected = viewModel.items[indexPath.item]
                itemscrenVC.viewModel = ItemScreenViewModel(id: Int(viewModel.items[indexPath.item].id!))
                itemscrenVC.viewModel._reference = selected
                viewModel.navigateTo(itemscrenVC, .push)
            }
    
            tableView.deselectRow(at: indexPath, animated: true)
        }
    
}

extension RecipeListTableViewController: CustomSearchBarDelagate{
    func onClickItemSuggestionsView(item: Item) {
        if  let itemscrenVC = storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
            itemscrenVC.viewModel = ItemScreenViewModel(id: Int(item.id!))
            itemscrenVC.viewModel._reference = item
            viewModel.navigateTo(itemscrenVC, .push)
            
        }
    }
    
    func onClickPostSuggestionsView(post: Post) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: post.permalink!)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: post.permalink!)!)
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func onClickShadowView(shadowView: UIView) {
        print("User touched shadowView")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Text did change, what i'm suppose to do ?")
    }
    
}
