//
//  SearchTTableViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 23.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
class SearchTableViewController: UITableViewController {
    
    let identifier = "searchCell"
    var searchBar:UISearchBar!
    var _searchViewModel = SearchViewModel()    
    var disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        tableView.register(UINib.init(nibName: "fromHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
        tableView.rowHeight = 60
        //tableView.estimatedRowHeight = 60
       tableView.delegate = nil
       tableView.dataSource = nil
       configureObserver()
        setBlur()
    }
    
    func setBlur(){
        let backView = UIVisualEffectView(frame: CGRect(x: (tableView?.bounds.origin.x)!, y: (tableView?.bounds.origin.y)!, width: (tableView?.bounds.width)!, height: (tableView?.bounds.height)!))
       backView.effect = UIBlurEffect(style: .extraLight)
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = backView
        tableView.tableFooterView = UIView()
    }
    
    
    func configureObserver(){
        
        _searchViewModel.filteredObservable.bind(to: tableView.rx.items(cellIdentifier: "searchCell", cellType: fromHistoryTableViewCell.self)){
            index,item,cell in
            cell.backgroundColor = UIColor.clear
           //cell.ConfigureView(reference: item)
            let title = item.name
            
            cell.resultLabel.text = title
            cell.viewTypeColor.backgroundColor = item.type.name()?.1
            cell.typeNameLabel.text = item.type.name()?.0
            cell.resultLabel.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
            
            ////

            }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Item.self).subscribe(onNext: { (item) in

                if  let itemscrenVC = self.storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
                    itemscrenVC.viewModel = ItemScreenViewModel(id: Int(item.id!))
                    itemscrenVC.viewModel._reference = item
                    self.dismiss(animated: true, completion: {
                        self._searchViewModel.navigateTo(itemscrenVC, .push)
                    })
                }
            //tableView.deselectRow(at: indexPath, animated: true)
           
        }).disposed(by: disposeBag)
        
        searchBar?.rx.text
            .orEmpty
            .distinctUntilChanged()
            .bind(to: (self._searchViewModel.query))
            .disposed(by:disposeBag)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    // MARK: - Table view data source

   

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
