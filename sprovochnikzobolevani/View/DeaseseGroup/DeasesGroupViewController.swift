//
//  DeasesGroupViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import SQLite
import Localize_Swift
import NVActivityIndicatorView

class DeasesGroupViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchBar: MySearchBar!
    
    var viewModel = CategoryViewModel()
    
    var searchViewModel = SearchViewModel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.backBarButtonItem?.title = "Directory".localized()
        self.title = "Diseases".localized()
        
        // search
        searchBar.backgroundImage = UIImage()
        searchBar.backgroundColor = UIColor.white
        searchBar.barTintColor = UIColor.white
        searchBar.placeholder = "Search by category".localized()
        
        searchBar.customSearchBarDelagate = self
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.borderWidth = 1
        tableView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        tableView.tableFooterView = UIView()
        tableView.rowHeight = 60
        
        // clear cell's left margins
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        setupData()
        
        if Localize.currentLanguage() == "ru" {
            self.searchViewModel.items = self.viewModel.items
        } else {
            self.searchViewModel.items = self.viewModel.items.filter { $0.type == .disease || $0.type == .procedure}
        }
        self.searchBar.setDatas(data: self.searchViewModel.items)
        
        //self.navigationController?.addSearchController(items: viewModel.items)
        
    }
    
    func setupData() {
        
        if isWritten {
            viewModel.categories = Category.get(lang: Localize.currentLanguage())
        }
        if !Connectivity.isConnectedToInternet && viewModel.categories.count == 0 {
            noNetworkConnectionAlert(self) {
                self.setupData()
            }
        } else if viewModel.categories.count == 0 {
            DiseaseServices.categories { r, categories in self.after(r) {
                if categories.count > 0 {
                    self.viewModel.categories.append(contentsOf: categories)
                    self.write {
                        DiseaseServices.saveCategories(table: Category.tableRu, categories: categories)
                    }
                }
                self.tableView.reloadData()
            }}
        }
        
    }
    
    func addSearchController(referencetype:ItemType){
        navigationController?.navigationBar.prefersLargeTitles = true
        let searchResultVc = storyboard?.instantiateViewController(withIdentifier: storyBoardIdentifier.searchTableViewResult) as? SearchTableViewController
        
        let searchController = UISearchController(searchResultsController: searchResultVc)
        searchResultVc?.searchBar = searchController.searchBar
        searchResultVc?._searchViewModel = SearchViewModel(items: [Item]())
        searchController.searchResultsController?.view.isHidden = false
        self.definesPresentationContext = true
        
        navigationItem.searchController = searchController
    }
}

extension DeasesGroupViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cathegorieCell")
        cell?.textLabel?.text = viewModel.categories[indexPath.item].name
        cell?.layoutMargins = UIEdgeInsets.zero
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if let referenceVC = storyboard?.instantiateViewController(withIdentifier: "referenceTable") as? RecipeListTableViewController{
            let vm = ListViewModel(viewModel.items.filter { $0.categoryId == viewModel.categories[indexPath.item].id })
            referenceVC.viewModel = vm
            referenceVC.viewModel.type = .disease
            referenceVC.title = viewModel.categories[indexPath.item].name
            viewModel.navigateTo(referenceVC, .push)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension DeasesGroupViewController: CustomSearchBarDelagate{
    func onClickItemSuggestionsView(item: Item) {
        if  let itemscrenVC = storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
            itemscrenVC.viewModel = ItemScreenViewModel(id: Int(item.id!))
            itemscrenVC.viewModel._reference = item
            viewModel.navigateTo(itemscrenVC, .push)
            
        }
    }
    
    func onClickPostSuggestionsView(post: Post) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: post.permalink!)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: post.permalink!)!)
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func onClickShadowView(shadowView: UIView) {
        print("User touched shadowView")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Text did change, what i'm suppose to do ?")
    }
    
}
