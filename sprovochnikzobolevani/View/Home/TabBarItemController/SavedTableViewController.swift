//
//  SavedTableViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 24.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SavedTableViewController: UITableViewController {

    var viewModel:SaveOrHistoryViewModel!
    
    public var suggestionListFiltered = SavedObject.get(type: .history).map({ (s) -> Item in
        return s.item()
    })
    
    var disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.backBarButtonItem?.title = "Directory".localized()
        tableView.register(UINib.init(nibName: "fromHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "cellIdentifier")
        tableView.rowHeight = 60.00
        tableView.delegate = nil
        tableView.dataSource = nil
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        configureObserver()
        
    }

    func configureObserver(){
        if let tableview = tableView{
            viewModel.savedObjects.asObservable().bind(to: tableview.rx.items(cellIdentifier: "cellIdentifier", cellType: fromHistoryTableViewCell.self)){
               (row,element,cell) in
                let item = element.item()
                //cell.ConfigureView(reference: ref!)
                
                cell.resultLabel.text = item.name//getFontAttribut(title: title, enteredLength: (entererWordLength)!) }
                cell.viewTypeColor.backgroundColor = item.type.name()?.1
                cell.typeNameLabel.text = item.type.name()?.0
                cell.resultLabel.backgroundColor = UIColor.clear
                cell.backgroundColor = UIColor.clear
                cell.selectionStyle = .none
                
                cell.layoutMargins = UIEdgeInsets.zero
                
                //
                }
                .disposed(by: disposeBag)
        }
       
        
        tableView.rx.modelSelected(SavedObject.self).subscribe(onNext: { (item) in
            let ref = item.item()

                if  let itemscrenVC = self.storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
                    itemscrenVC.viewModel = ItemScreenViewModel()
                    itemscrenVC.viewModel._reference = ref
                    self.viewModel?.navigateTo(itemscrenVC, .push)
                }
            
        }).disposed(by: disposeBag)
       
        //Delete
        tableView.rx.modelDeleted(SavedObject.self).subscribe(onNext: { (s) in
          let selectedIndex  =  self.viewModel.savedObjects.value.index(of: s)
            //
            self.viewModel.savedObjects.value.remove(at: selectedIndex!)
            SavedObject.remove(id: s.id!, type: self.viewModel.type)
        }).disposed(by: disposeBag)
        tableView.tableFooterView = UIView()
        
        
        //Nav Bts
        let eraseAllBts = UIBarButtonItem(title: "Clear".localized(), style: .plain, target: self, action: nil)
        
        
        eraseAllBts.rx.tap.subscribe(onNext: { (_) in
          
            self.present(self.alertDisplay(), animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        viewModel.savedObjects.asObservable().subscribe(onNext: { (s) in
            (!s.isEmpty) ? self.navigationItem.rightBarButtonItem = eraseAllBts : self.navigationItem.rightBarButtonItems?.removeAll()
        }).disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.viewModel.type == .bookmark{
            viewModel.savedObjects.value = SavedObject.get(type: .bookmark)
        }
       
    }
    
    func alertDisplay() -> UIAlertController{
        
        let titleType:String = (self.viewModel.type == .bookmark) ? "Bookmarks".localized() : "History".localized()
        var clear = "Clear".localized()
        clear.append(" \(titleType)")
        var qquesion = "Do you really want to clean up".localized()
        qquesion.append(" \(titleType)?")
        let _alert = UIAlertController(title: clear, message: qquesion, preferredStyle: .alert)
        let _yesAction = UIAlertAction(title:"Clear".localized(), style: UIAlertActionStyle.default) { (_) in
              self.viewModel.savedObjects.value.removeAll()
            SavedObject.delete(type: self.viewModel.type)
        }
        
        
        let _noAction = UIAlertAction(title: "Cancel".localized(), style: .default) { (_) in
            _alert.dismiss(animated: true, completion: nil)
        }
        _alert.addAction(_noAction)
        _alert.addAction(_yesAction)
        _alert.preferredAction = _yesAction
        return _alert
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.suggestionListFiltered.count == 0 {
            return 1
        }
        
        return self.suggestionListFiltered.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell") as? fromHistoryTableViewCell
        
        let title = self.suggestionListFiltered[indexPath.row].name
        
        cell?.resultLabel.text = title//getFontAttribut(title: title, enteredLength: (entererWordLength)!) }
        cell?.viewTypeColor.backgroundColor = self.suggestionListFiltered[indexPath.row].type.name()?.1
        cell?.typeNameLabel.text = self.suggestionListFiltered[indexPath.row].type.name()?.0
        cell?.resultLabel.backgroundColor = UIColor.clear
        cell?.backgroundColor = UIColor.clear
        cell?.selectionStyle = .default
        
        return cell!
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
