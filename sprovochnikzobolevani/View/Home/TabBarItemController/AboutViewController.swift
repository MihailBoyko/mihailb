//
//  AboutViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 30.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import SafariServices

class AboutViewController: UIViewController {

    @IBOutlet weak var DirectoryOfDiseases: UILabel!
    @IBOutlet weak var version: UILabel!
    @IBOutlet weak var pubCard:CardVIew!
    @IBOutlet weak var ourApp: UILabel!
    @IBOutlet weak var OnCooperationIssues: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.backBarButtonItem?.title = "Directory".localized()
        
        setText()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func pushLinkButton(_ sender: Any) {
        let appID = 1272223825
        let urlStr = "itms-apps://itunes.apple.com/app/id\(appID)"
        // (Option 1) Open App Page
        // let urlStr = "https://itunes.apple.com/app/viewContentsUserReviews?id=\(appID)" // (Option 2) Open App Review Tab
        if let url = URL(string: urlStr) { //, UIApplication.shared.canOpenURL(url) {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "webView") as? WebViewController{
                vc.url = url
               // (UIApplication.topViewController()?.navigationController)?.navigationItem.searchController = nil
                (UIApplication.topViewController()?.navigationController)?.pushViewController(vc, animated: true)
            }
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
        }
    }
    func setText()  {
        DirectoryOfDiseases.text = "Directory of Diseases".localized()
        version.text = "version 0.1".localized()
        ourApp.text = "Our application is created for convenient and free search of the information interesting you. We will be grateful if you will help the development of the project, leaving a positive feedback and assessment on".localized()
        OnCooperationIssues.text = "On cooperation Issues".localized()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func preparePubCardView(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(gotToAnalog))
        self.pubCard.addGestureRecognizer(tap)
    }
    
    @objc func gotToAnalog(){
        let appID = 1272223825
        let urlStr = "itms-apps://itunes.apple.com/app/id\(appID)"
        // (Option 1) Open App Page
        // let urlStr = "https://itunes.apple.com/app/viewContentsUserReviews?id=\(appID)" // (Option 2) Open App Review Tab
        
        
        if let url = URL(string: urlStr) { //}, UIApplication.shared.canOpenURL(url) {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "webView") as? WebViewController{
                vc.url = url
                (UIApplication.topViewController()?.navigationController)?.pushViewController(vc, animated: true)
            }
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
        }
    }

}
