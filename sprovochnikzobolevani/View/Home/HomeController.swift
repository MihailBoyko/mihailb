//
//  ViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 14.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Localize_Swift
import SQLite
import NVActivityIndicatorView
import SafariServices

class HomeController: UIViewController, UITabBarDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var Shuffle: UIView!
    @IBOutlet weak var tabHistory: UITabBarItem!
    @IBOutlet weak var textDisease: UILabel!
    @IBOutlet weak var textProcedure: UILabel!
    @IBOutlet weak var textDiet: UILabel!
    @IBOutlet weak var tabBookmark: UITabBarItem!
    @IBOutlet weak var tabLanguage: UITabBarItem!
    @IBOutlet weak var tabAbout: UITabBarItem!
    @IBOutlet weak var cardMessage: UILabel!
    @IBOutlet weak var cardFooter: UIButton!
    @IBOutlet weak var cardHeader: UILabel!
    @IBOutlet weak var greenBtn: CardVIew!
    @IBOutlet weak var YellowBtn: CardVIew!
    @IBOutlet weak var ProcedureImage: UIImageView!
    @IBOutlet weak var dietImage: UIImageView!
    @IBOutlet weak var searchBar:MySearchBar!
    @IBOutlet weak var pubCard:CardVIew!
    
    var viewModel = HomeViewModel()
    var searchViewModel = SearchViewModel()
 
    
    enum AnimType {
        case circle
        case orbit
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        
        // fix black background artifact
        navigationController?.view?.backgroundColor = UIColor.white

        // search
        searchBar.backgroundImage = UIImage()
        searchBar.backgroundColor = UIColor.white
        searchBar.barTintColor = UIColor.white
        
        searchBar.customSearchBarDelagate = self
        
        tabBar.delegate = self
        preparePubCardView()
        
        setText()
        
        
        
    }
    
    @objc func showCircleLoader() {
        let size = CGSize(width: 100, height: 100)
        
        self.startAnimating(size, message: "Language changing...".localized(), type: NVActivityIndicatorType.ballScaleRippleMultiple)
    }
    
    @objc func showOrbitLoader() {
        let size = CGSize(width: 100, height: 100)
        
        self.startAnimating(size, message: "Wait please...".localized(), type: NVActivityIndicatorType.ballScaleRippleMultiple)
    }
    
    
    func setText() {
        
        cardFooter.setTitle("Show and download".localized(),for: .normal )
        searchBar.placeholder = "Directory Search".localized()
        title = "Directory".localized()
        tabBookmark.title = "Bookmarks".localized()
        tabHistory.title = "History".localized()
        tabAbout.title = "About".localized()
        tabLanguage.title = "Language".localized()
        cardMessage.text = "Cheap analogs of drugs, instructions, reviews".localized()
        cardHeader.text = "Diseases Dictionary".localized()
        textDiet.text = "Diets".localized()
        textDisease.text = "Diseases".localized()
        textProcedure.text = "Procedures".localized()
        
        if Localize.currentLanguage() == "en" {
            greenBtn.backgroundColor = greenBtn.backgroundColor?.withAlphaComponent(0.5)
            YellowBtn.backgroundColor = YellowBtn.backgroundColor?.withAlphaComponent(1)
            textDiet.backgroundColor = textDiet.backgroundColor?.withAlphaComponent(0)
            textProcedure.backgroundColor = textProcedure.backgroundColor?.withAlphaComponent(1)
            ProcedureImage.alpha = 1
            dietImage.alpha = 0.5
        }else if Localize.currentLanguage() == "ru"{
            greenBtn.backgroundColor = greenBtn.backgroundColor?.withAlphaComponent(1)
            YellowBtn.backgroundColor = YellowBtn.backgroundColor?.withAlphaComponent(1)
            textDiet.backgroundColor = textDiet.backgroundColor?.withAlphaComponent(1)
            textProcedure.backgroundColor = textProcedure.backgroundColor?.withAlphaComponent(1)
            ProcedureImage.alpha = 1
            dietImage.alpha = 1
        } else if Localize.currentLanguage() == "es"{
            greenBtn.backgroundColor = greenBtn.backgroundColor?.withAlphaComponent(0.5)
             YellowBtn.backgroundColor = YellowBtn.backgroundColor?.withAlphaComponent(0.5)
            textDiet.backgroundColor = textDiet.backgroundColor?.withAlphaComponent(0)
            textProcedure.backgroundColor = textProcedure.backgroundColor?.withAlphaComponent(0)
            ProcedureImage.alpha = 0.5
            dietImage.alpha = 0.5
        }
        
        
        for i in self.childViewControllers{
            if(i is ExempleViewController){
                (i as! ExempleViewController).unShuffl()
                (i as! ExempleViewController).setText()
            }
        }
        
        for i in self.childViewControllers{
            if(i is ExempleViewController && (i as! ExempleViewController).viewModel.items.count > 4){
                (i as! ExempleViewController).suffle()
            }
        }
        
    }
    
    func updateData() {
        
        if Localize.currentLanguage() == "en" || Localize.currentLanguage() == "es" {
            pubCard.isHidden = true
        } else {
            pubCard.isHidden = false
        }
        navigationController?.popToRootViewController(animated: true)
        write {
            
            let table: Table
            let table2: Table
            switch Localize.currentLanguage() {
                case "ru":
                    table = Item.tableEn
                    table2 = Item.tableEs
                case "en":
                    table = Item.tableRu
                    table2 = Item.tableEs
                case "es":
                    table = Item.tableEn
                    table2 = Item.tableRu
                default :
                    table = Item.tableRu
                    table2 = Item.tableEs
            }
            
            if Localize.currentLanguage() == "es"{
                UserDefaults.standard.set(0, forKey: "ru.involta.diseasesEnTimestamp")
                UserDefaults.standard.set(0, forKey: "ru.involta.diseasesRuTimestamp")
            }else if Localize.currentLanguage() == "ru" {
                UserDefaults.standard.set(0, forKey: "ru.involta.diseasesEsTimestamp")
                UserDefaults.standard.set(0, forKey: "ru.involta.diseasesEnTimestamp")
            }else {
                UserDefaults.standard.set(0, forKey: "ru.involta.diseasesEsTimestamp")
                UserDefaults.standard.set(0, forKey: "ru.involta.diseasesRuTimestamp")
            }
        
            try DataBaseManager.shared.db.run(table.filter(Item.typeField == ItemType.disease.rawValue).delete())
            try DataBaseManager.shared.db.run(table2.filter(Item.typeField == ItemType.disease.rawValue).delete())
        }

            DispatchQueue.main.async {
               self.setText()
                
                self.viewModel.items = [Item]()
                if isWritten {
                    self.viewModel.items.append(contentsOf: Item.get(lang: Localize.currentLanguage()))
                }
                if Connectivity.isConnectedToInternet && self.viewModel.items.count >= 0 {
                    DiseaseServices.diseases { r, items in self.after(r) {
                        if (items.count > 0) {
                            self.viewModel.items.append(contentsOf: items)
                            let table: Table
                            switch Localize.currentLanguage() {
                                case "ru": table = Item.tableRu
                                case "en": table = Item.tableEn
                                case "es": table = Item.tableEs
                                default : table = Item.tableEn
                            }
                            self.write {
                                DiseaseServices.saveItems(table: table, items: items)
                            }
                        }
                        if Localize.currentLanguage() == "ru" {
                            self.searchViewModel.items = self.viewModel.items
                        }else if Localize.currentLanguage() == "es"{
                            self.searchViewModel.items = self.viewModel.items.filter { $0.type == .disease }
                        } else {
                            self.searchViewModel.items = self.viewModel.items.filter { $0.type == .disease || $0.type == .procedure}
                        }
                        self.searchBar.setDatas(data: self.searchViewModel.items)
                        for i in self.childViewControllers{
                            if(i is ExempleViewController){
                                (i as! ExempleViewController).viewModel.items = [Item]()
                                (i as! ExempleViewController).viewModel.items = self.viewModel.items.filter { $0.type == .disease }
                                (i as! ExempleViewController).suffle()
                            }
                        }
                    }}
                    if Localize.currentLanguage() == "en" {
                        ProcedureServices.procedures { r2, items in self.after(r2) {
                            if items.count > 0 {
                                self.viewModel.items.append(contentsOf: items)
                                let table = Localize.currentLanguage() == "ru" ? Item.tableRu : Item.tableEn
                                self.write {
                                    DiseaseServices.saveItems(table: table, items: items)
                                }
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                }
                            }
                        }}
                    } else {
                        DispatchQueue.main.async {
                            self.stopAnimating()
                        }
                    }
                } else if !Connectivity.isConnectedToInternet && self.viewModel.items.count == 0 {
//                    self.noNetworkConnectionAlert(self) {
//                        self.updateData()
//                    }
                }
            }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            if !Connectivity.isConnectedToInternet {
                self.noNetworkConnectionAlert(self, nil)
            }else{
                self.showOrbitLoader()
                self.updateData()
            }
        }
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        switch tabBar.items?.index(of: item) {
        case 0:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "savedVC") as? SavedTableViewController{
                vc.viewModel = SaveOrHistoryViewModel(type: .bookmark)
                vc.title = item.title
                viewModel.navigateTo(vc, .push)
            }
            break
        case 1:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "savedVC") as? SavedTableViewController{
                vc.viewModel = SaveOrHistoryViewModel(type: .history)
                vc.title = item.title
                viewModel.navigateTo(vc, .push)
            }
        case 2:
            switchLanguageAlert(self) {
                self.updateData()
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                    if !Connectivity.isConnectedToInternet {
                        self.noNetworkConnectionAlert(self, nil)
                    }else{
                        self.showCircleLoader()
                    }
                }
            }
            break
        case 3:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "aboutVC") as? AboutViewController{
                vc.navigationItem.title = item.title
                viewModel.navigateTo(vc, .push)
                return
            }
            break
        default:
            return
        }
    }
  
    @IBAction func homeCardClicked(_ sender:CardVIew){
        if Localize.currentLanguage() == "ru" {
            if  let _toVC = (storyboard?.instantiateViewController(withIdentifier: "deseaseCathegorieVC"))! as? DeasesGroupViewController {
                let vm = CategoryViewModel(viewModel.items.filter { $0.type == .disease })
                _toVC.viewModel = vm
                viewModel.navigateTo(_toVC, .push)
            }
        } else {
            if let referenceVC = storyboard?.instantiateViewController(withIdentifier: "dictionaryViewController") as? DictionaryViewController {
                let vm = DictionaryViewModel(viewModel.items.filter { $0.type == .disease })
                referenceVC.viewModel = vm
                viewModel.navigateTo(referenceVC, .push)
            }
        }
    }
    @IBAction func procedureBtsClicked(_ sender: CardVIew){
        if (Localize.currentLanguage() == "ru") {
            if let referenceVC = storyboard?.instantiateViewController(withIdentifier: "referenceTable") as? RecipeListTableViewController{
                let vm = ListViewModel(viewModel.items.filter { $0.type == .procedure})
                vm.type = .procedure
                referenceVC.viewModel = vm
                referenceVC.title = "Procedures".localized()
                viewModel.navigateTo(referenceVC, .push)
                
            }
        } else if Localize.currentLanguage() == "en" {
            if let referenceVC = storyboard?.instantiateViewController(withIdentifier: "procedureViewController") as? ProcedureViewController {
                let vm = DictionaryViewModel(viewModel.items.filter { $0.type == .procedure})
                referenceVC.viewModel = vm
                viewModel.navigateTo(referenceVC, .push)
            }
        }
        
        
       
    }
    
    @IBAction func dietBtsClicked(_ sender: CardVIew) {
        if (Localize.currentLanguage() == "ru") {
            if let referenceVC = storyboard?.instantiateViewController(withIdentifier: "referenceTable") as? RecipeListTableViewController{
                let vm = ListViewModel(viewModel.items.filter { $0.type == .diet })
                vm.type = .diet
                referenceVC.viewModel = vm
                referenceVC.title = "Diets".localized()
                viewModel.navigateTo(referenceVC, .push)
            }
        }
    
    }
    
    func preparePubCardView(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(gotToAnalog))
        self.pubCard.addGestureRecognizer(tap)
    }
    @IBAction func pushBtnGoToAnalog(_ sender: Any) {
        let appID = 1272223825
        let urlStr = "itms-apps://itunes.apple.com/app/id\(appID)"
        // (Option 1) Open App Page
        // let urlStr = "https://itunes.apple.com/app/viewContentsUserReviews?id=\(appID)" // (Option 2) Open App Review Tab
        
        
        if let url = URL(string: urlStr) {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "webView") as? WebViewController{
                vc.url = url
                (UIApplication.topViewController()?.navigationController)?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func gotToAnalog(){
        let appID = 1272223825
        let urlStr = "itms-apps://itunes.apple.com/app/id\(appID)"
        // (Option 1) Open App Page
        // let urlStr = "https://itunes.apple.com/app/viewContentsUserReviews?id=\(appID)" // (Option 2) Open App Review Tab
        
        
        if let url = URL(string: urlStr) {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "webView") as? WebViewController{
                vc.url = url
                (UIApplication.topViewController()?.navigationController)?.pushViewController(vc, animated: true)
            }
            
//            let svc = SFSafariViewController(url: url)
//            self.present(svc, animated: true, completion: nil)
            
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
        }
    }
}

extension HomeController: CustomSearchBarDelagate{
    func onClickItemSuggestionsView(item: Item) {
            if  let itemscrenVC = storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
                itemscrenVC.viewModel = ItemScreenViewModel(id: Int(item.id!))
                itemscrenVC.viewModel._reference = item
                viewModel.navigateTo(itemscrenVC, .push)
                
            }
    }
    
    func onClickPostSuggestionsView(post: Post) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "webView") as? WebViewController{
            vc.url = URL(string: post.permalink!)
            (UIApplication.topViewController()?.navigationController)?.pushViewController(vc, animated: true)
        }
//        if #available(iOS 10.0, *) {
//            UIApplication.shared.open(URL(string: post.permalink!)!, options: [:], completionHandler: nil)
//        } else {
//            UIApplication.shared.openURL(URL(string: post.permalink!)!)
//        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func onClickShadowView(shadowView: UIView) {
        print("User touched shadowView")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Text did change, what i'm suppose to do ?")
    }
    
}

