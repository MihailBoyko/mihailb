//
//  CardVIew.swift
//  newAnalog
//
//  Created by GETHEME on 27.02.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit

@IBDesignable class CardVIew: UIControl {
    @IBInspectable var cornerRadius:CGFloat = 24
    @IBInspectable var shadowOffsetWidth:CGFloat = 0
    @IBInspectable var shadowOffSetHeigth:CGFloat = 2
    @IBInspectable var shadowColor:UIColor = UIColor.black
    @IBInspectable var shadowOpacity:Float = 0.12
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffSetHeigth)
//        let rect = CGRect(x: 0, y: 2, width: shadowOffsetWidth, height: shadowOffSetHeigth)
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.shadowPath = shadowPath.cgPath
        layer.shadowRadius = 4
        layer.shadowOpacity = shadowOpacity
    }

}
