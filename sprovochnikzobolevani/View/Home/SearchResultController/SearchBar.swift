import UIKit
import Foundation
import Localize_Swift

  protocol CustomSearchBarDelagate: UISearchBarDelegate{
      func onClickShadowView(shadowView: UIView)
     func onClickItemSuggestionsView(item: Item)
    func onClickPostSuggestionsView(post: Post)
}


class MySearchBar: UISearchBar,UISearchBarDelegate,UITableViewDataSource, UITableViewDelegate {
    

    private var posts: [Post] = [Post]()
    
    //Mark:Configuration
    public var customSearchBarDelagate: CustomSearchBarDelagate?
    //MARK: DATAS
    private var isSuggestionsViewOpened:Bool!
    
    private var suggestionList = [Item]()
    
    public var suggestionListFiltered = SavedObject.get(type: .history).map({ (s) -> Item in
       return s.item()
    })
    
    private var keyboardHeight: CGFloat = 0
    private var isSearching:Bool = false
    
    //MARKS : VIEWS
    var suggestionsView: UITableView!
    private var suggestionsShadow: UIVisualEffectView!
    
    
    //Public Propretie
    public var shadowView_alpha: CGFloat = 0.3
    
    //Label
    public var searchLabel_font: UIFont?
    public var searchLabel_textColor: UIColor?
    public var searchLabel_BackgroundColor: UIColor?
    public var entererWordLength:NSRange?
    public var suggestionsView_maxHeight: CGFloat!
    public var suggestionsView_backgroundColor: UIColor?
    public var suggestionView_contentViewColor: UIColor?
    public var suggestionView_separatorStyle: UITableViewCellSeparatorStyle = UITableViewCellSeparatorStyle.singleLine
    public var suggestionView_selectionStyle: UITableViewCellSelectionStyle = UITableViewCellSelectionStyle.default
    public var suggestionView_verticalSpaceWithSearchBar: CGFloat = 0
    
    //    public var suggestionsView_searchIcon_height: CGFloat = 17
    //    public var suggestionsView_searchIcon_width: CGFloat = 17
    //    public var suggestionsView_searchIcon_isRound = true
    
    public var suggestionsView_spaceWithKeyboard:CGFloat = 3
    
    //MARK: INITIALISERS
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
        
              setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
            setupView()
    }
    
    init () {
        super.init(frame: CGRect.zero)
        setup()
        setupView()
    }
    
    private func setup(){
        self.delegate = self
        self.isSuggestionsViewOpened = false
        self.interceptOrientationChange()
        self.interceptKeyboardChange()
        self.interceptMemoryWarning()
    }
    
    @IBInspectable var TextFieldradius:CGFloat = 10{
        didSet{
            self.setupView()
        }
    }
    @IBInspectable var shadowOffsetWidth:CGFloat = 4{
        
        didSet{
            self.setupView()
        }
    }
    
    @IBInspectable var shadowOffSetHeigth:CGFloat = 3 {
        
        didSet{
            self.setupView()
        }
    }
    @IBInspectable var shadowColor:UIColor = UIColor.black {
        
        didSet{
            self.setupView()
        }
    }
    @IBInspectable var shadowOpacity:CGFloat = 0.8 {
        
        didSet{
            self.setupView()
        }
    }
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    func setupView(){
        
        //        layer.borderWidth = 0
        setupTextFielsView()
        
        
        //        layer.borderColor = UIColor.clear.cgColor
        
    }
    
    func setupTextFielsView()
    {
        if let index = indexOfSearchFieldInSubviews(){
            let searchField:UITextField  = (subviews[0] as UIView).subviews[index] as! UITextField
            // searchField.layer.cornerRadius = searchField.bounds.height/2
            searchField.layer.borderWidth = 2
            searchField.layer.cornerRadius = 10
            searchField.layer.borderColor = UIColor(red: 43/255, green: 123/255, blue: 255/255, alpha: 1).cgColor
            searchField.layer.shadowOffset = CGSize(width: shadowOffsetWidth - 1, height: shadowOffSetHeigth)
            let shadowPath = UIBezierPath(roundedRect: searchField.bounds, cornerRadius: TextFieldradius)
            
            searchField.layer.shadowPath = shadowPath.cgPath
            searchField.layer.shadowColor = UIColor.green.cgColor
            searchField.layer.shadowOpacity = Float(shadowOpacity)
        }
    }
    
    func indexOfSearchFieldInSubviews() -> Int! {
        var indexOfSearchField: Int!
        let searchBarView = self.subviews[0]
        
        for (index,v) in searchBarView.subviews.enumerated(){
            if v.isKind(of: UITextField.self)
            {
                indexOfSearchField = index
            }
        }
        
        return indexOfSearchField
    }

    
    private func configurationViews(){
        
        //Configuration Shadow (Behind the tableView)
      
      
       
        //        blurEffectView.layer.borderColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1).cgColor
    
        self.suggestionsShadow = UIVisualEffectView(frame: CGRect(x: getShadowX(), y: getShadowY(), width: getShadowWidth(), height: getShadowHeight()))
        self.suggestionsShadow.effect = UIBlurEffect(style: .extraLight)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = suggestionsShadow.frame
          self.suggestionsShadow.layer.borderWidth = 0.5
         self.suggestionsShadow.layer.borderColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1).cgColor
//        self.suggestionsShadow = blurEffectView
        
        
        
//          blurEffectView.frame = self.suggestionsView.frame
//
//        self.suggestionsShadow = blurEffectView
        
        //        self.suggestionsShadow.backgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 0.78)
        
        //Configuration gesture handel (click on shadow an improve focus on searchBar
        let gestureShadow = UITapGestureRecognizer(target: self, action: #selector(self.onClickShadowView (_:)))
        self.suggestionsShadow.addGestureRecognizer(gestureShadow)
        let gestureRemoveFocus = UITapGestureRecognizer(target: self, action: #selector(self.removeFocus(_:)))
        gestureRemoveFocus.cancelsTouchesInView = false
        
        //Configure suggestionView
        self.suggestionsView = UITableView(frame: CGRect(x: getSuggestionsViewX(), y: getSuggestionsViewY(), width: getSuggestionsViewWidth(), height: 0))
        self.suggestionsView.delegate = self
        self.suggestionsView.dataSource = self
        
        self.suggestionsView.backgroundColor = UIColor.clear
        
        self.suggestionsView.register(UINib.init(nibName: "fromHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "searchCell")
        self.suggestionsView.register(UINib.init(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "postCell")
        
        self.suggestionsView.rowHeight = UITableViewAutomaticDimension
        self.suggestionsView.estimatedRowHeight = 100
        self.suggestionsView.separatorStyle = self.suggestionView_separatorStyle
        self.suggestionsView.layoutMargins = UIEdgeInsets.zero
        self.suggestionsView.separatorInset = UIEdgeInsets.zero
        if let backgroundColor = suggestionsView_backgroundColor
        {
            self.suggestionsView.backgroundColor = backgroundColor
        }
        
        
        
        self.getTopViewController()?.view.addGestureRecognizer(gestureRemoveFocus)
        
        ///Reload datas of suggestionsView
        self.suggestionsView.reloadData()
    }
    
    // --------------------------------
    // SET DATAS
    // --------------------------------
    
    public func setDatas(data:[Item]){
        self.suggestionList = data
    }
    
    // --------------------------------
    // DELEGATE METHODS SEARCH BAR
    // --------------------------------
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isSearching = true

        self.searchWhenUserTyping(caracters: searchText)
        self.customSearchBarDelagate?.searchBar?(searchBar, textDidChange: searchText)
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if self.suggestionsView == nil { self.configurationViews() }
        self.customSearchBarDelagate?.searchBarTextDidEndEditing?(searchBar)
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = nil
        self.closeSuggestionsView()
        self.customSearchBarDelagate?.searchBarTextDidEndEditing?(searchBar)
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            DiseaseServices.search(query: self.characters) { (er, diseases, posts) in
                self.posts = posts
                if posts.count > 0 || self.suggestionListFiltered.count > 0 {
                    self.isSearching = true
                } else if posts.count == 0 && self.suggestionListFiltered.count == 0{
                    self.isSearching = false
                }
                self.suggestionListFiltered.removeAll()
                if (self.posts.count == 0) {
                    for d in diseases {
                        self.suggestionListFiltered.append(Item(id: Int64(d.id!), categoryId: Int64(d.category!), name: d.name!, type: .disease, lang: Localize.currentLanguage()))
                        }
                }
                
                self.suggestionsView.reloadData()
                self.updateSizeSuggestionsView()
            }
        
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.closeSuggestionsView()
        self.customSearchBarDelagate?.searchBarCancelButtonClicked?(searchBar)
        searchBar.text = nil
        searchBar.reloadInputViews()
        searchBar.resignFirstResponder()
    }
    
    
    public func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        isSearching = false
        if let shouldEndEditing = self.customSearchBarDelagate?.searchBarShouldEndEditing?(searchBar) {
            suggestionListFiltered = SavedObject.get(type: .history).map({ (s) -> Item in
                return s.item()
            })
                        self.suggestionsView.reloadData()
                        self.openSuggestionsView()
            return shouldEndEditing
        } else {
            return true
        }
    }
    
    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if let shouldBeginEditing = self.customSearchBarDelagate?.searchBarShouldBeginEditing?(searchBar) {
            self.suggestionListFiltered = SavedObject.get(type: .history).map({ (s) -> Item in
                return s.item()
            })
            self.suggestionsView.reloadData()
            self.openSuggestionsView()
            return shouldBeginEditing
        } else {

            searchBar.setShowsCancelButton(true, animated: true)
            return true
        }
    }
    
    // --------------------------------
    // SEARCH FUNCTION
    // --------------------------------
    
    ///Main function that is called when user searching
    private func searchWhenUserTyping(caracters: String){
        isSearching = true
        var suggestionListFiltredTmp = [Item]()
        var suggestionListFiltredTmpBegin = [Item]()
        
        DispatchQueue.global(qos: .background).async {
            for item in  self.suggestionList
            {
                if(self.researchCaractersBegin(stringSearched: caracters, stringQueried: item.name))
                {
                    suggestionListFiltredTmpBegin.append(item)
                    
                }else if(self.researchCaracters(stringSearched: caracters, stringQueried: item.name))
                {
                    suggestionListFiltredTmp.append(item)
                    
                }
            }
            DispatchQueue.main.async {
                self.suggestionListFiltered.removeAll()
                self.suggestionListFiltered.append(contentsOf: suggestionListFiltredTmpBegin)
                self.suggestionListFiltered.append(contentsOf: suggestionListFiltredTmp)
                self.updateAfterSearch(caracters: caracters)
            }
        }
        
        
    }
    
    private func researchCaracters(stringSearched: String, stringQueried: String) -> Bool {
        return stringQueried.lowercased().contains(stringSearched.lowercased())
    }
    
    private func researchCaractersBegin(stringSearched: String, stringQueried: String) -> Bool {
        if stringSearched.count > 0 && stringSearched.count <= stringQueried.count{
            let stringSearchedTemp = Array(stringSearched.lowercased())
            let stringQueriedTemp = Array(stringQueried.lowercased())
            for i in 0...stringSearchedTemp.count - 1{
                if stringSearchedTemp[i] != stringQueriedTemp[i]{
                    return false
                }
            }
            return true
        }
        return false
    }
    
    var characters: String = ""
    
    private func updateAfterSearch(caracters: String){
        self.characters = caracters
        if (suggestionListFiltered.count > 0) {
            posts = [Post]()
        }
        self.suggestionsView.reloadData()
        self.updateSizeSuggestionsView()
        
        caracters.isEmpty ? self.closeSuggestionsView(): self.openSuggestionsView()
    }
    
    //    private func showHistory(){
    //        if let history = SettingStorage.getHistory() {
    //            suggestionListFiltered = history
    //            isSearching = false
    //            suggestionsView.reloadData()
    //            self.isSuggestionsViewOpened = true
    //        }
    //        else{
    //            closeSuggestionsView()
    //        }
    //    }
    // --------------------------------
    // ACTIONS
    // --------------------------------
    
    ///Handle click on shadow view
    @objc func onClickShadowView(_ sender:UITapGestureRecognizer){
        self.customSearchBarDelagate?.onClickShadowView(shadowView: self.suggestionsShadow)
        self.closeSuggestionsView()
    }
    
    ///Remove focus when you tap outside the searchbar
    @objc func removeFocus(_ sender:UITapGestureRecognizer){
        if (!isSuggestionsViewOpened){ self.endEditing(true) }
    }
    
    
    
    
    
    
    // --------------------------------
    // DELEGATE METHODS TABLE VIEW
    // --------------------------------
    
    //MARK: TableView Delagate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.posts.count > 0 {
            return self.posts.count + 1
        }
        
        if (self.suggestionListFiltered.count > 0) {
            return self.suggestionListFiltered.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (posts.count > 0 || suggestionListFiltered.count == 0) {
            if (indexPath.row == 0) {
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cellempty")
            cell.layoutMargins = UIEdgeInsets.zero
            if(Connectivity.isConnectedToInternet && (self.text?.count)! > 0 && posts.count == 0){
                if !isSearching {
                    cell.textLabel?.text = "Nothing was found".localized()
                } else {
                    cell.textLabel?.text = "".localized()
                }
                cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                cell.textLabel?.textAlignment = NSTextAlignment.center;
                cell.textLabel?.numberOfLines = 0
            } else if posts.count > 0 {
                cell.textLabel?.text = "Sorry, your search did not find anything, but we have prepared a couple of articles for you".localized()
                cell.textLabel?.textAlignment = NSTextAlignment.center;
                cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                cell.textLabel?.numberOfLines = 0
            } else if !Connectivity.isConnectedToInternet {
                cell.textLabel?.text = "No network connection!".localized()
                cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                cell.textLabel?.textAlignment = NSTextAlignment.center;
                cell.textLabel?.numberOfLines = 0
            }
            cell.textLabel?.font = UIFont(name: "HelveticaNeue", size: 17)
            cell.textLabel?.textColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1)
            cell.textLabel?.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.clear
            cell.isUserInteractionEnabled = false
            cell.layoutMargins = UIEdgeInsets.zero
            cell.selectionStyle = .none
            return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "postCell") as? PostTableViewCell
                
                let title = self.posts[indexPath.row - 1].postTitle!
                
                // if( (title != "" || !title.isEmpty) && entererWordLength != nil){
                
                cell?.name.text = title//getFontAttribut(title: title, enteredLength: (entererWordLength)!) }
                cell?.preview.text = self.posts[indexPath.row - 1].preview?.trimmingCharacters(in: .whitespacesAndNewlines)
                
                cell?.selectionStyle = .none
                //cell?.viewTypeColor.backgroundColor = UIColor.white
                
                //cell?.accesoiryIcon.image = isSearching ? #imageLiteral(resourceName: "newsearch"): #imageLiteral(resourceName: "HistoryIcon")
                
                ///Configure label
                //        cell.textLabel?.text = title
                //        if let font = self.searchLabel_font { cell.textLabel?.font = font }
                //        if let textColor = self.searchLabel_textColor { cell.textLabel?.textColor = textColor }
                //        if let backgroundColor = self.searchLabel_BackgroundColor { cell.textLabel?.backgroundColor = UIColor.clear }
                //cell?.resultLabel.backgroundColor = UIColor.clear
                ///Configure content
                //        if let contentColor = suggestionView_contentViewColor { cell.contentView.backgroundColor = UIColor.clear }
                cell?.backgroundColor = UIColor.clear
                cell?.selectionStyle = .default
                
                //        ///Configure Image
                //        cell.configureImage(choice: self.choice, searchImage: self.searchImage, suggestionsListWithUrl: self.suggestionListWithUrlFiltred, position: indexPath.row, isImageRound: self.suggestionsView_searchIcon_isRound, heightImage: self.suggestionsView_searchIcon_height)
                //        //self.fetchImageFromUrl(model: self.suggestionListWithUrlFiltred[indexPath.row], cell: cell, indexPath: indexPath)
                //
                //        ///Configure max width label size
                //        cell.configureWidthMaxLabel(suggestionsViewWidth: self.suggestionsView.frame.width, searchImageWidth: self.suggestionsView_searchIcon_width)
                //
                //        ///Configure Constraints
                //        cell.configureConstraints(heightImage: self.suggestionsView_searchIcon_height, widthImage: self.suggestionsView_searchIcon_width)
                
                return cell!
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell") as? fromHistoryTableViewCell
        
        cell?.selectionStyle = .none
        
        let title = self.suggestionListFiltered[indexPath.row].name
        
       // if( (title != "" || !title.isEmpty) && entererWordLength != nil){
            
        cell?.resultLabel.text = title//getFontAttribut(title: title, enteredLength: (entererWordLength)!) }
        cell?.viewTypeColor.backgroundColor = self.suggestionListFiltered[indexPath.row].type.name()?.1
        cell?.typeNameLabel.text = self.suggestionListFiltered[indexPath.row].type.name()?.0
        
        //cell?.accesoiryIcon.image = isSearching ? #imageLiteral(resourceName: "newsearch"): #imageLiteral(resourceName: "HistoryIcon")
        
        ///Configure label
        //        cell.textLabel?.text = title
        //        if let font = self.searchLabel_font { cell.textLabel?.font = font }
        //        if let textColor = self.searchLabel_textColor { cell.textLabel?.textColor = textColor }
        //        if let backgroundColor = self.searchLabel_BackgroundColor { cell.textLabel?.backgroundColor = UIColor.clear }
        cell?.resultLabel.backgroundColor = UIColor.clear
        ///Configure content
        //        if let contentColor = suggestionView_contentViewColor { cell.contentView.backgroundColor = UIColor.clear }
        cell?.backgroundColor = UIColor.clear
        
        //        ///Configure Image
        //        cell.configureImage(choice: self.choice, searchImage: self.searchImage, suggestionsListWithUrl: self.suggestionListWithUrlFiltred, position: indexPath.row, isImageRound: self.suggestionsView_searchIcon_isRound, heightImage: self.suggestionsView_searchIcon_height)
        //        //self.fetchImageFromUrl(model: self.suggestionListWithUrlFiltred[indexPath.row], cell: cell, indexPath: indexPath)
        //
        //        ///Configure max width label size
        //        cell.configureWidthMaxLabel(suggestionsViewWidth: self.suggestionsView.frame.width, searchImageWidth: self.suggestionsView_searchIcon_width)
        //
        //        ///Configure Constraints
        //        cell.configureConstraints(heightImage: self.suggestionsView_searchIcon_height, widthImage: self.suggestionsView_searchIcon_width)
        cell?.layoutMargins = UIEdgeInsets.zero
        return cell!
    }
    
    func getFontAttribut(title:String,enteredLength:NSRange) -> NSAttributedString{
        let attributedString = NSMutableAttributedString(string: title, attributes: [
            NSAttributedStringKey.font: UIFont(name: "HelveticaNeue", size: 17.0)!,
            NSAttributedStringKey.foregroundColor: UIColor(red: 123/255, green: 123/255, blue: 129/255, alpha: 1)
            ])
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(white: 0.0, alpha: 1.0), range: enteredLength)
        return attributedString
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row  == 0 && posts.count > 0  {
            return 120
        }
        return 60
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (self.suggestionListFiltered.count == 0) {
            self.customSearchBarDelagate?.onClickPostSuggestionsView(post: self.posts[indexPath.row - 1])
        } else {
            self.customSearchBarDelagate?.onClickItemSuggestionsView(item: self.suggestionListFiltered[indexPath.row])
        }
        isSearching = false
        tableView.reloadData()
        if let cell = tableView.cellForRow(at: indexPath){
            cell.isSelected = false
        }
        self.resignFirstResponder()
    }
    
    
    //MARK: UISearch Customisation
    
    
    //    func indexOfSearchFieldInSubviews() -> Int! {
    //        var indexOfSearchField: Int!
    //        let searchBarView = subviews[0]
    //
    //        for (index,v) in searchBarView.subviews.enumerated(){
    //            if v.isKind(of: UITextField.self)
    //            {
    //                indexOfSearchField = index
    //            }
    //        }
    //
    //        return indexOfSearchField
    //    }
    //
    override func draw(_ rect: CGRect){
        
        
        
    }
    
    
    // --------------------------------
    // VIEW UTILS
    // --------------------------------
    
    private func getSuggestionsViewX() -> CGFloat {
        return getShadowX()
    }
    
    private func getSuggestionsViewY() -> CGFloat {
        return self.getShadowY()
    }
    
    private func getSuggestionsViewWidth() -> CGFloat {
        return self.getShadowWidth()
    }
    
    private func getSuggestionsViewHeight() -> CGFloat {
        return self.getEditText().frame.height
    }
    
    private func getShadowX() -> CGFloat {
        return 0
    }
    
    private func getShadowY() -> CGFloat {
       return self.frame.origin.y + 60
//        return self.getGlobalPointEditText().y  + self.getEditText().frame.height
    }
    
    private func getShadowHeight() -> CGFloat {
        return (self.getTopViewController()?.view.frame.height)!
    }
    
    private func getShadowWidth() -> CGFloat {
        return (self.getTopViewController()?.view.frame.width)!
    }
    
    private func updateSizeSuggestionsView(){
        var frame: CGRect = self.suggestionsView.frame
        frame.size.height = self.getExactMaxHeightSuggestionsView(newHeight: self.suggestionsView.contentSize.height)
        
        UIView.animate(withDuration: 0.3) {
            self.suggestionsView.frame = frame
            self.suggestionsView.layoutIfNeeded()
            self.suggestionsView.sizeToFit()
        }
    }
    
    private func getExactMaxHeightSuggestionsView(newHeight: CGFloat) -> CGFloat {
        var estimatedMaxView: CGFloat!
        if self.suggestionsView_maxHeight != nil {
            estimatedMaxView = self.suggestionsView_maxHeight
        } else {
            estimatedMaxView = self.getEstimateHeightSuggestionsView()
        }
        
        if (newHeight > estimatedMaxView) {
            return estimatedMaxView
        } else {
            return newHeight
        }
    }
    
    private func getEstimateHeightSuggestionsView() -> CGFloat {
        return self.getViewTopController().frame.height
            - self.getShadowY()
            - self.keyboardHeight
            - self.suggestionsView_spaceWithKeyboard
    }
    
    
    // --------------------------------
    // OBSERVERS CHANGES
    // --------------------------------
    
    private func interceptOrientationChange(){
        self.getEditText().addObserver(self, forKeyPath: "frame", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "frame"){
            self.closeSuggestionsView()
            self.configurationViews()
        }
    }
    
    // ---------------
    
    private func interceptKeyboardChange(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: NSObject] as NSDictionary
        let keyboardFrame = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! CGRect
        let keyboardHeight = keyboardFrame.height
        
        self.keyboardHeight = keyboardHeight
        self.updateSizeSuggestionsView()
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        self.keyboardHeight = 0
        self.updateSizeSuggestionsView()
    }
    
    // ---------------
    
    private func interceptMemoryWarning(){
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveMemoryWarning(notification:)), name: NSNotification.Name.UIApplicationDidReceiveMemoryWarning, object: nil)
    }
    
    @objc private func didReceiveMemoryWarning(notification: NSNotification) {
        self.clearCacheOfList()
    }
    
    
    // --------------------------------
    //  UTILS
    // --------------------------------
    
    // MARK: Restore Data
    
    
    private func clearCacheOfList(){
        
        self.suggestionsView.reloadData()
    }
    
    private func addViewToParent(view: UIView){
        if let topController = getTopViewController() {
            let superView: UIView = topController.view
            superView.addSubview(view)
        }
    }
    
    private func getViewTopController() -> UIView{
        return self.getTopViewController()!.view
    }
    
    // Get Top controller
    private func getTopViewController() -> UIViewController?{
        
//        var topcontroller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
//
//        while topcontroller?.presentedViewController != nil {
//            topcontroller = topcontroller?.presentedViewController
//        }
//        return  topcontroller
      return  UIApplication.topViewController()
    }
    
    private func getEditText() -> UITextField
    {
        return self.value(forKey: "searchField") as! UITextField
    }
    
    private func getText() -> String{
        if let text = self.getEditText().text {
            return text
        } else {
            return ""
        }
    }
    
    private func getGlobalPointEditText() -> CGPoint {
        return self.getEditText().superview!.convert(self.getEditText().frame.origin, to: nil)
    }
    // --------------------------------
    // SUGGESTIONS VIEW UTILS
    // --------------------------------
    
    private func haveToOpenSuggestionView() -> Bool {
        //        return !self.suggestionListFiltered.isEmpty
        return true
    }
    
    
    func openSuggestionsView(){
        if (self.haveToOpenSuggestionView()){
            
            if (!self.isSuggestionsViewOpened){
                self.animationOpening()
                self.setShowsCancelButton(true, animated: true)
                self.addViewToParent(view: self.suggestionsShadow)
                self.addViewToParent(view: self.suggestionsView)
                self.isSuggestionsViewOpened = true
                self.suggestionsView.reloadData()
            }
        }
    }
    
  
    public func closeSuggestionsView(){
        if (self.isSuggestionsViewOpened == true){
            posts = [Post]()
            self.suggestionsView.reloadData()
            self.animationClosing()
            self.isSuggestionsViewOpened = false
            self.resignFirstResponder()
            self.entererWordLength = nil
            self.setShowsCancelButton(false, animated: true)
        }
    }
    
    private func animationOpening(){
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [], animations: {
            self.suggestionsView.alpha = 1.0
            self.suggestionsShadow.alpha = 1.0
        }, completion: nil)
       
    }
    
    private func animationClosing(){
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [], animations: {
            self.suggestionsView.alpha = 0.0
            self.suggestionsShadow.alpha = 0.0
        }, completion: nil)
    }
    
    // --------------------------------
    // PUBLIC ACCESS
    // --------------------------------
    
    public func getSuggestionsView() -> UITableView {
        return self.suggestionsView
    }
}

