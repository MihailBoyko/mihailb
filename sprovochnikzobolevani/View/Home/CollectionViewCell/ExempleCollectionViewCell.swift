//
//  ExempleCollectionViewCell.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit

class ExempleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var viewCard:CardVIew!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.autoresizingMask = [.flexibleWidth]
        
        // Initialization code
    }
    
    override func layoutSubviews() {
        self.nameLabel.preferredMaxLayoutWidth = 64.00
    }
    
    
}
