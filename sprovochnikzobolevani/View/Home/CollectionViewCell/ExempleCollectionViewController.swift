//
//  ExempleCollectionViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ExempleCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout{

    var referencetype:ItemType?
    var viewModel:ExempleCollectionViewModel?
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = ExempleCollectionViewModel()
        if let r = referencetype{
               viewModel?.referencetype = r
        }
     
        
       viewModel?.populateReference()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        self.collectionView?.delegate = self
        // Register cell classes
        self.collectionView!.register(UINib.init(nibName: "ExempleCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "exempleCell")
        self.collectionView!.register(UINib.init(nibName: "ShuffleCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shuffleBtn")
       self.collectionView?.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return (viewModel?.numberOfElement())!
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath == IndexPath(item: 0, section: 0){
          let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shuffleBtn", for: indexPath) as? ShuffleCollectionViewCell
            cell?.shuffleBts.addTarget(self, action: #selector(shufflleNow), for: UIControlEvents.touchUpInside)
            return cell!
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "exempleCell", for: indexPath) as? ExempleCollectionViewCell
        cell?.nameLabel.text = viewModel?.references[indexPath.item - 1].name
        cell?.nameLabel.textColor = viewModel?.backgroundText
        cell?.viewCard.backgroundColor = viewModel?.backgroundColor
        cell?.setNeedsLayout()
        cell?.layoutIfNeeded()
        // Configure the cell
    
        return cell!
    }

    
    @objc func shufflleNow(){
        viewModel?.populateReference()
        collectionView?.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath != IndexPath(item: 0, section: 0){
            var cell: ExempleCollectionViewCell? = self.offscreenCells[reuseIdentifier] as? ExempleCollectionViewCell
            if cell == nil {
                cell = Bundle.main.loadNibNamed("ExempleCollectionViewCell", owner: self, options: nil)![0] as? ExempleCollectionViewCell
                self.offscreenCells[reuseIdentifier] = cell
            }
            cell?.nameLabel.text = viewModel?.references[indexPath.item - 1].name
            cell?.viewCard.backgroundColor = viewModel?.backgroundColor
            cell?.nameLabel.textColor = viewModel?.backgroundText
            let targetheigth:CGFloat = 32.00
            
            cell?.bounds = CGRect(x:0, y:0, width:cell!.bounds.width, height: targetheigth)
            
            cell?.contentView.bounds = cell!.bounds
//            cell?.viewCard.setNeedsLayout()
//            cell?.viewCard.layoutIfNeeded()
            cell?.setNeedsLayout()
            cell?.layoutIfNeeded()
            var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
            // Still need to force the width, since width can be smalled due to break mode of labels
            size.height = targetheigth
            return size
        }
        return CGSize(width: 40, height: 40)
    }
  
   
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

            if  let itemscrenVC = storyboard?.instantiateViewController(withIdentifier: "itemScreen") as? ItemScreenTableViewController{
                let selected = viewModel?.references[indexPath.item - 1]
                itemscrenVC.viewModel = ItemScreenViewModel(id: Int((viewModel?.references[indexPath.item - 1].id)!))
                itemscrenVC.viewModel._reference = selected
                viewModel?.navigateTo(itemscrenVC, .push)
            }
            collectionView.resignFirstResponder()
        //tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
