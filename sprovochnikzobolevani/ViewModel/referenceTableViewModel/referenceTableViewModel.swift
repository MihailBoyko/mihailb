//
//  referenceTableViewModel.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 17.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
class ReferenceTableViewModel: BaseViewModel {
    var reference: [Item]!
    var id: Int?
    init(id: Int?) {
        self.id = id
    }
    
    func numberOfItems()->Int{
        return reference.count
    }
}
