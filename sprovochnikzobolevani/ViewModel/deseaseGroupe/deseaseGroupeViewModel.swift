//
//  deseaseGroupeViewModel.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import Alamofire
import Gloss
import SQLite
import Localize_Swift

class deseaseGroupViewModel: BaseViewModel{
    var groups:[Category]
    override init() {
        groups = Category.get(lang: Localize.currentLanguage())
        super.init()
    }
    
    func reload() {
        groups = Category.get(lang: Localize.currentLanguage())
    }

    

    func numberOfItem()-> Int{
        return groups.count
    }

    func getSubElement(id:Int)->[Item]{
        return Item.get(categoryId: id, lang: Localize.currentLanguage())
    }
    
}
