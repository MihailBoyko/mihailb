
import Foundation
import RxSwift

class SearchViewModel: BaseViewModel {
    
    let disposeBag = DisposeBag()
    
    var query = Variable("")
    
    var all = Variable([Item]())
    var filtered = Variable([Item]())
    
    lazy var queryObservable = query.asObservable()
    lazy var allObservable = all.asObservable()
    lazy var filteredObservable = filtered.asObservable()
    
    var items = [Item]() {
        didSet(value) {
            all = Variable(value)
            allObservable = all.asObservable()
        }
    }
    
    override init() {
        super.init()
    }
    
    init(items: [Item]) {
        
        super.init()
        
        self.all = Variable(items)
        
        queryObservable.subscribe(onNext: { (query) in
            
            self.allObservable.map {$0.filter {
                
                if query.isEmpty {
                    return true
                }
            
                return $0.name.lowercased().contains(query.lowercased())
                
            }}.bind(to: self.filtered).disposed(by: self.disposeBag)
            
        }).disposed(by: disposeBag)
        
    }
    
}
