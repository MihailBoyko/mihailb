//
//  BaseViewController.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import UIKit

enum navigationType {
    case modal
    case push
}

class BaseViewModel: NSObject, router {

    func navigateTo(_ toController: UIViewController, _ typeNavigation: navigationType) {

        let topController = UIApplication.topViewController()
        
        switch typeNavigation {
        case .modal:
            topController?.present(topController!, animated: true, completion:nil)
        case .push:
            (topController?.navigationController)?.navigationItem.searchController = nil
            (topController?.navigationController)?.pushViewController(toController, animated: true)
        }
        
    }
    
}
