
import Foundation

class CategoryViewModel: BaseViewModel {
    
    var items = [Item]()
    var categories = [Category]()
    
    override init() {
        super.init()
    }
    
    init(_ items: [Item]) {
        self.items = items
    }
    
}
