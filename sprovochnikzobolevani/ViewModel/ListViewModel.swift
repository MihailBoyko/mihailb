
import Foundation

class ListViewModel: BaseViewModel {
    
    var type: ItemType = .diet
    var items = [Item]()
    
    override init() {
        super.init()
    }
    
    init(_ items: [Item]) {
        self.items = items
    }
}
