
import Foundation

class HomeViewModel: BaseViewModel {
    
    var items = [Item]()
    var categorys = [Category]()
    
    override init() {
        super.init()
    }
    
    init(_ items: [Item], categorys: [Category]) {
        self.items = items
        self.categorys = categorys
    }
}
