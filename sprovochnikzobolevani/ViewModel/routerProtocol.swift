//
//  routerProtocol.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import UIKit

protocol router {
    func navigateTo(_ toController:UIViewController, _ typeNavigation:navigationType)
}
