//
//  SaveOrHistoryViewModel.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 24.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import RxSwift

class SaveOrHistoryViewModel: BaseViewModel {
    
    let type: SavedObjectType
    
    lazy var savedObjects:Variable<[SavedObject]> = Variable(SavedObject.get(type: type))
    lazy var savedObjectsReference:Observable<[SavedObject]> = savedObjects.asObservable()
    
    init(type: SavedObjectType) {
        self.type = type
        
    }
    
}
