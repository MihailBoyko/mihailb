//
//  File.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import UIKit
import Localize_Swift



class ExempleCollectionViewModel:BaseViewModel{
    
    var referencetype:ItemType? = nil
    var forHomeScreen:Bool = true
    var backgroundColor : UIColor = UIColor(red: 43/255, green: 123/255, blue: 255/255, alpha: 1)
    
    var backgroundText: UIColor = UIColor.white
      var references:[Item] = [Item]()

    override init() {
        super.init()
        

    }
   
      public func populateReference(){

        references = populatedeaseas().randomget(randomPick: 4)
        
    }
    
    
    private func populatedeaseas()-> [Item]{

        if let r = referencetype {
            return Item.get(type: r, lang: Localize.currentLanguage()).randomget(randomPick: 4)
        } else {
            return Item.get(lang: Localize.currentLanguage()).randomget(randomPick: 4)
        }
        
    }
   
    private func populateForHomeScreen()-> [Item]{
        var array:[Item] = [Item]()
        
        let procedures = Item.get(type: .procedure, lang: Localize.currentLanguage()).randomget(randomPick: 2)
        
        procedures.forEach({ (p) in
            array.append(p)
        })
        
        let deseases = Item.get(type: .disease, lang: Localize.currentLanguage()).randomget(randomPick: 1)
        deseases.forEach({ (d) in
            array.append(d)
        })
       
        let diets = Item.get(type: .diet, lang: Localize.currentLanguage()).randomget(randomPick: 1)
        diets.forEach({ (d) in
            array.append(d)
        })
        return array
    }
    
    func numberOfElement ()->Int{
        return (references.count) + 1
    }
    
    
//    private func populate(referenceType:referenceType)->[reference]{
//
//    }
}
