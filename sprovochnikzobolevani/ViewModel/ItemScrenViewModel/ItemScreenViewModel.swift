//
//  ItemScreenViewModel.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 18.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ItemScreenViewModel: BaseViewModel {
    
    var _reference:Item!
    var hasImage:Bool = false
    
    lazy var isSaved:Variable<Bool> = Variable(self._reference.isSaved)
    
    var id: Int?
    
    init(id: Int){
        self.id = id
    }
    
    override init() {
        
        super.init()
    }
    func numberofContentSection()->Int{
        return _reference.content.count
    }
    
    func saveInHystory(){
        SavedObject.save(item: _reference, type: .history)
    }
    
}
