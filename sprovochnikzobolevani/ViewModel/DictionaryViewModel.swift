
import Foundation

class DictionaryViewModel: BaseViewModel {
    
    var items = [Item]()
    var categories = [Category]()
    
    override init() {
        super.init()
    }
    
    init(_ items: [Item]) {
        self.items = items
    }
    
    init(_ items: [Item], _ categories: [Category]) {
        self.items = items
        self.categories = categories
    }
    
}
