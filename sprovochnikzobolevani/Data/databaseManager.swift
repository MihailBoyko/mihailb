//
//  databaseManager.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation
import SQLite

class DataBaseManager{
  
    public static let shared = DataBaseManager()
    public  var db:Connection!
    
    private let dbPath:String = {
        let fileManager = FileManager.default
        
        let dbPath = try! fileManager
            .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            .appendingPathComponent("db.sqlite")
            .path
        
        if !fileManager.fileExists(atPath: dbPath) {
            let dbResourcePath = Bundle.main.path(forResource: "testDB", ofType: "sqlite")!
            try! fileManager.copyItem(atPath: dbResourcePath, toPath: dbPath)
        }
        
        return dbPath
    }()
    
    
    
     init() {
       
      try! self.openDataBase()
       
    }
    
    
    public  func openDataBase()throws {
       
            self.db = try! Connection(self.dbPath)
            print("connection  to database susseced")
       
    }
}
