//
//  dataBaseProtocol.swift
//  sprovochnikzobolevani
//
//  Created by GETHEME on 15.04.2018.
//  Copyright © 2018 GETHEME. All rights reserved.
//

import Foundation

 protocol DatabaseManagebal{
    associatedtype T
     static func findAll() -> [T]?
}
